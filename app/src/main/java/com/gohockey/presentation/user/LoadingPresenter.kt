package com.gohockey.presentation.user

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gohockey.App
import com.gohockey.model.FirestoreCollections
import com.gohockey.model.IceOffer
import com.gohockey.model.old.Admin
import com.gohockey.model.old.GoalKeeper
import com.gohockey.model.old.Player
import com.gohockey.ui.user.Screens
import com.google.firebase.firestore.FirebaseFirestore

@InjectViewState
class LoadingPresenter(
    private val userId: String,
    private val requestOffer: IceOffer?
) : MvpPresenter<LoadingView>() {

    private val firestore = FirebaseFirestore.getInstance()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        firestore.collection(FirestoreCollections.USERS)
            .document(userId)
            .get()
            .addOnSuccessListener {
                when (it["idRole"] as? String) {
                    "Администратор" -> {
                        firestore.collection(FirestoreCollections.ADMIN)
                            .document(userId)
                            .get()
                            .addOnSuccessListener {
                                App.router.newRootScreen(
                                    Screens.Admin(
                                        Admin(
                                            additaionInformation = (it["additaionInformation"] as? String) ?: "",
                                            avatarUrl = (it["avatarUrl"] as? String) ?: "",
                                            idRole = it["idRole"] as String,
                                            metroId = (it["metroId"] as? String) ?: "",
                                            name = (it["name"] as? String) ?: "",
                                            surname = (it["surname"] as? String) ?: "",
                                            userId = it["userId"] as String,
                                            work = (it["work"] as? String) ?: "",
                                            telephone = (it["telephone"] as? String) ?: ""
                                        ),
                                        requestOffer
                                    )
                                )
                            }
                    }
                    "Игрок" -> {
                        firestore.collection(FirestoreCollections.PLAYER)
                            .document(userId)
                            .get()
                            .addOnSuccessListener {
                                App.router.newRootScreen(
                                    Screens.Player(
                                        Player(
                                            age = (it["age"] as? String) ?: "",
                                            avatarUrl = (it["avatarUrl"] as? String) ?: "",
                                            handSide = (it["handSide"] as Long).toInt(),
                                            height = (it["height"] as? String) ?: "",
                                            idRole = it["idRole"] as String,
                                            metroId = (it["metroId"] as? String) ?: "",
                                            name = (it["name"] as? String) ?: "",
                                            qualification = (it["qualification"] as? String) ?: "",
                                            surname = (it["surname"] as? String) ?: "",
                                            userId = it["userId"] as String,
                                            weight = (it["weight"] as? String) ?: "",
                                            role = (it["role"] as? String) ?: ""
                                        ),
                                        requestOffer
                                    )
                                )
                            }
                    }
                    "Вратарь" -> {
                        firestore.collection(FirestoreCollections.KEEPER)
                            .document(userId)
                            .get()
                            .addOnSuccessListener {
                                App.router.newRootScreen(
                                    Screens.GoalKeeper(
                                        GoalKeeper(
                                            age = (it["age"] as? String) ?: "",
                                            avatarUrl = (it["avatarUrl"] as? String) ?: "",
                                            handSide = (it["handSide"] as Long).toInt(),
                                            height = (it["height"] as? String) ?: "",
                                            idRole = it["idRole"] as String,
                                            metroId = (it["metroId"] as? String) ?: "",
                                            name = (it["name"] as? String) ?: "",
                                            priceFrom = (it["priceFrom"] as? String) ?: "0",
                                            priceTo = (it["priceTo"] as? String) ?: "0",
                                            qualification = (it["qualification"] as? String) ?: "",
                                            surname = (it["surname"] as? String) ?: "",
                                            userId = it["userId"] as String,
                                            weight = (it["weight"] as? String) ?: "",
                                            isVisible = (it["isVisible"] as? Boolean) ?: false
                                        ),
                                        requestOffer
                                    )
                                )
                            }
                    }
                    else -> {
                        viewState.showMessage("Не удалось загрузить")
                    }
                }
            }
    }
}