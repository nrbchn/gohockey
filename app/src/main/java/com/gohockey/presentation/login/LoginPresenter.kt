package com.gohockey.presentation.login

import android.util.Patterns
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.google.firebase.auth.FirebaseAuth

@InjectViewState
class LoginPresenter : MvpPresenter<LoginView>() {

    private val auth = FirebaseAuth.getInstance()

    fun login(login: String, password: String) {
        if (login.isEmpty()) {
            viewState.showAlert(message = "Введите e-mail")
        } else if (!Patterns.EMAIL_ADDRESS.matcher(login).matches()) {
            viewState.showAlert(message = "Неправильный формат e-mail")
        } else if (password.isEmpty()) {
            viewState.showAlert(message = "Введите пароль")
        } else {
            viewState.showProgress(true)
            auth.signInWithEmailAndPassword(login, password)
                .addOnSuccessListener { viewState.showMainScreen() }
                .addOnFailureListener { viewState.showAlert(message = it.localizedMessage) }
                .addOnCompleteListener { viewState.showProgress(false) }
        }
    }

    fun sendNewPassword(email: String) {
        if (email.isEmpty()) {
            viewState.showAlert(message = "Введите e-mail")
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            viewState.showAlert(message = "Неправильный формат e-mail")
        } else {
            viewState.showProgress(true)
            auth.sendPasswordResetEmail(email)
                .addOnSuccessListener {
                    viewState.showAlert(
                        "Проверьте почту",
                        "Письмо с восстановлением пароля было отправлено вам на почту"
                    )
                }
                .addOnFailureListener { viewState.showAlert(message = it.localizedMessage) }
                .addOnCompleteListener { viewState.showProgress(false) }
        }
    }
}