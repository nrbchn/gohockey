package com.gohockey.presentation.login

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface LoginView : MvpView {
    fun showMainScreen()
    fun showAlert(title: String = "", message: String)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showProgress(show: Boolean)
}