package com.gohockey.presentation.main.addpost

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface AddPostView : MvpView {
    fun showData(data: List<Any>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showAddPostScreen()
}