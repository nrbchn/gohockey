package com.gohockey.presentation.main.addpost

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gohockey.model.PostCategory

@InjectViewState
class AddPostPresenter : MvpPresenter<AddPostView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.showData(listOf(
//            PostCategory.Team()
            PostCategory.Ice()
//            PostCategory.Match(),
//            PostCategory.Tackle(),
//            PostCategory.Cup(),
//            PostCategory.Rink()
        ))
    }

    fun onCategoryClicked(category: PostCategory) {
        viewState.showAddPostScreen()
    }

    fun onBackPressed() {

    }
}