package com.gohockey.presentation.main.feed

import android.location.Location
import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gohockey.model.*
import com.gohockey.model.old.Metro
import com.gohockey.ui.main.feed.FeedFragment
import com.gohockey.ui.main.feed.IceOfferItem
import com.gohockey.ui.map.Marker
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

@InjectViewState
class FeedPresenter(
    private val prefs: Prefs,
    private val fusedLocationProviderClient: FusedLocationProviderClient
): MvpPresenter<FeedView>() {

    private val firestore = FirebaseFirestore.getInstance()
    private val data = mutableListOf<IceOfferItem>()
    private var disposable: Disposable? = null
    private var currentSortType = FeedFragment.Sort.DATE

    fun refresh() {
        disposable?.dispose()
        disposable = getCurrentLocation()
            .subscribeOn(Schedulers.io())
            .flatMap { getIceOffers(it) }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { iceOffers ->
                    data.clear()
                    data.addAll(iceOffers)
                    viewState.showError(if (data.isEmpty()) "Публикаций нет" else null)
                    sort(currentSortType)
                },
                {
                    Log.e("FeedPresenter", "refresh error", it)
                    viewState.showData(emptyList())
                    viewState.showError("Не удалось загрузить")
                }
            )
    }

    fun sort(sortType: FeedFragment.Sort) {
        currentSortType = sortType
        when (sortType) {
            FeedFragment.Sort.DATE -> viewState.showData(data.sortedByDescending { it.post.createdAt })
            FeedFragment.Sort.START -> viewState.showData(data.sortedBy { it.post.startDate })
            FeedFragment.Sort.PRICE -> viewState.showData(data.sortedBy { it.post.price.toInt() })
            FeedFragment.Sort.METRO -> viewState.showData(data.sortedBy { it.distance })
        }
    }

    fun openMap(postId: String? = null) {
        viewState.showMap(
            if (postId.isNullOrEmpty()) {
                val markers = mutableListOf<Marker>()
                data.forEach { dataItem ->
                    val metro = Metro.getMetroById(dataItem.post.metroId)
                    markers.add(
                        Marker(
                            lat = metro.lat,
                            lon = metro.lng,
                            user = dataItem.author,
                            iceOffer = dataItem.post
                        )
                    )
                }
                markers
            } else {
                data.filter { postId == it.post.id }.map { dataItem ->
                    val metro = Metro.getMetroById(dataItem.post.metroId)
                    val marker = Marker(
                        lat = metro.lat,
                        lon = metro.lng,
                        user = dataItem.author,
                        iceOffer = dataItem.post
                    )
                    marker
                }
            }
        )
    }

    private fun getCurrentLocation(): Single<LatLng> =
        Single.create<LatLng> { emitter ->
            try {
                fusedLocationProviderClient.lastLocation
                    .addOnSuccessListener { location ->
                        if (emitter.isDisposed) {
                            return@addOnSuccessListener
                        }

                        if (location != null) {
                            val latlng = LatLng(location.latitude, location.longitude)
                            prefs.lastLocation = latlng
                            emitter.onSuccess(latlng)
                        } else {
                            emitter.onSuccess(prefs.lastLocation)
                        }
                    }
                    .addOnFailureListener { }
            } catch (exception: SecurityException) {
                emitter.onSuccess(prefs.lastLocation)
            }
        }

    private fun getIceOffers(location: LatLng): Single<List<IceOfferItem>> =
        Single.create<List<IceOfferItem>> { emitter ->
            Tasks.whenAllSuccess<QuerySnapshot>(
                firestore.collection(FirestoreCollections.ICE_OFFERS).get(),
                firestore.collection(FirestoreCollections.PLAYER).get(),
                firestore.collection(FirestoreCollections.ADMIN).get(),
                firestore.collection(FirestoreCollections.KEEPER).get()
            )
                .addOnSuccessListener { snapshots ->
                    if (emitter.isDisposed) {
                        return@addOnSuccessListener
                    }

                    val iceOffers = mutableListOf<IceOfferItem>()
                    snapshots[0].forEach { document ->
                        try {
                            val post = document.data.toIceOffer()
                            val metro = Metro.getMetroById(post.metroId)

                            val userSnapshot =
                                snapshots[1].documents.find { it.data?.get("userId") == post.userId } ?:
                                snapshots[2].documents.find { it.data?.get("userId") == post.userId } ?:
                                snapshots[3].documents.find { it.data?.get("userId") == post.userId }

                            userSnapshot?.let {
                                val user = User(
                                    id = userSnapshot.get("userId").toString(),
                                    name = userSnapshot.get("name").toString() + " " + userSnapshot.get("surname").toString(),
                                    photo = userSnapshot.get("avatarUrl") as? String
                                )
                                val dist = distance(
                                    location.latitude, location.longitude,
                                    metro.lat, metro.lng
                                )
                                iceOffers.add(IceOfferItem(user, post, dist))
                            }
                        } catch (e: Exception) {
                            Log.e("FeedPresenter", e.message, e)
                        }
                    }

                    emitter.onSuccess(iceOffers)
                }
                .addOnFailureListener { emitter.onError(it) }
        }

    override fun onDestroy() {
        disposable?.dispose()
        super.onDestroy()
    }

    companion object {
        fun distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
            val result = FloatArray(1)
            Location.distanceBetween(lat1, lon1, lat2, lon2, result)
            return result[0].toDouble() / 1000
        }
    }
}