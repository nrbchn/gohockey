package com.gohockey.presentation.main.messages

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gohockey.model.FirestoreCollections
import com.gohockey.model.message.MessageItem
import com.gohockey.model.message.Message
import com.gohockey.model.User
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import java.text.SimpleDateFormat
import java.util.*

@InjectViewState
class MessagesPresenter : MvpPresenter<MessagesView>() {

    private val formatter = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
    private val currentUser = FirebaseAuth.getInstance().currentUser
    private val database = FirebaseDatabase.getInstance()
    private val firestore = FirebaseFirestore.getInstance()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        refresh()
    }

    fun refresh() {
        if (currentUser?.uid == null) {
            //todo
        } else {
            viewState.showProgress(true)

            database.reference
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val messageKeys = dataSnapshot.child("user-messages")
                            .child(currentUser.uid)
                            .children
                            .map { it.children.last().key!! }

                        val messages = dataSnapshot.child("messages")
                            .children
                            .filter { messageKeys.contains(it.key)}
                            .map { it.getValue(Message::class.java) }
                            .filterNotNull()

                        Tasks.whenAllSuccess<QuerySnapshot>(
                            firestore.collection(FirestoreCollections.PLAYER).get(),
                            firestore.collection(FirestoreCollections.KEEPER).get(),
                            firestore.collection(FirestoreCollections.ADMIN).get()
                        )
                            .addOnSuccessListener {
                                val data = messages.map { message ->
                                    val users = mutableListOf<User>()
                                    it.forEach {
                                        users.addAll(
                                            it.documents.map {
                                                User(
                                                    it.get("userId") as String,
                                                    it.get("name") as String + " " + it.get("surname") as String,
                                                    it.get("avatarUrl") as String?
                                                )
                                            }
                                        )
                                    }

                                    val userId = if (currentUser.uid == message.toId) message.fromId else message.toId
                                    val iam = currentUser.uid == message.fromId
                                    val user = users.find { it.id == userId }!!

                                    MessageItem(
                                        user,
                                        (if (iam) "Вы: " else "") + message.text,
                                        formatter.format(Date(message.timestamp * 1000))
                                    )
                                }

                                viewState.showData(data.reversed())
                                if (data.isEmpty()) {
                                    viewState.showError("Сообщений нет")
                                } else {
                                    viewState.showError(null)
                                }
                            }
                            .addOnFailureListener {
                                viewState.showData(emptyList())
                                viewState.showError(it.localizedMessage)
                            }
                            .addOnCompleteListener { viewState.showProgress(false) }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.e("MessagesPresenter","loadPost:onCancelled", error.toException())
                        viewState.showData(emptyList())
                        viewState.showError(error.message)
                        viewState.showProgress(false)
                    }
                })
        }
    }
}