package com.gohockey.presentation.requests

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gohockey.model.FirestoreCollections
import com.gohockey.model.User
import com.gohockey.model.toIceOffer
import com.gohockey.ui.requests.RequestItem
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot

@InjectViewState
class RequestsPresenter : MvpPresenter<RequestsView>() {

    private val currentUser = FirebaseAuth.getInstance().currentUser
    private val firestore = FirebaseFirestore.getInstance()
    private var task = false

    fun refresh() {
        if (task) return
        task = true

        if (currentUser == null) {
            //todo
        } else {
            viewState.showProgress(true)
            Tasks.whenAllSuccess<QuerySnapshot>(
                firestore.collection(FirestoreCollections.ICE_OFFERS).get(),
                firestore.collection(FirestoreCollections.PLAYER).get(),
                firestore.collection(FirestoreCollections.KEEPER).get(),
                firestore.collection(FirestoreCollections.ADMIN).get()
            )
                .addOnSuccessListener {
                    val iceOffers = it[0].documents
                        .map { it.data }
                        .filter { it != null && it["userId"] == currentUser.uid && (it["membersWait"] as List<*>).isNotEmpty() }
                        .map { it!!.toIceOffer() }

                    val data = mutableListOf<RequestItem>()
                    iceOffers.forEach { iceOffer ->
                        iceOffer.membersWait.forEach { userId ->
                            it[1].documents
                                .filter { it["userId"] == userId }
                                .map {
                                    User(
                                        it.get("userId") as String,
                                        it.get("name") as String + " " + it.get("surname") as String,
                                        it.get("avatarUrl") as String?
                                    )
                                }
                                .forEach { user -> data.add(RequestItem(user, iceOffer)) }

                            it[2].documents
                                .filter { it["userId"] == userId }
                                .map {
                                    User(
                                        it.get("userId") as String,
                                        it.get("name") as String + " " + it.get("surname") as String,
                                        it.get("avatarUrl") as String?
                                    )
                                }
                                .forEach { user -> data.add(RequestItem(user, iceOffer)) }

                            it[3].documents
                                .filter { it["userId"] == userId }
                                .map {
                                    User(
                                        it.get("userId") as String,
                                        it.get("name") as String + " " + it.get("surname") as String,
                                        it.get("avatarUrl") as String?
                                    )
                                }
                                .forEach { user -> data.add(RequestItem(user, iceOffer)) }
                        }
                    }

                    viewState.showRequests(data.sortedByDescending { it.iceOffer.createdAt })
                    if (data.isEmpty()) {
                        viewState.showError("Заявок нет")
                    } else {
                        viewState.showError(null)
                    }
                }
                .addOnFailureListener {
                    viewState.showRequests(emptyList())
                    viewState.showError(it.message)
                }
                .addOnCompleteListener {
                    viewState.showProgress(false)
                    task = false
                }
        }
    }
}