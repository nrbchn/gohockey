package com.gohockey.presentation.requests

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.gohockey.ui.requests.RequestItem

@StateStrategyType(AddToEndSingleStrategy::class)
interface RequestsView : MvpView {
    fun showRequests(requests: List<RequestItem>)
    fun showError(message: String?)
    fun showProgress(show: Boolean)
}