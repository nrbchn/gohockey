package com.gohockey.presentation.chat

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface UsersView : MvpView {
    fun showData(data: List<Any>)
    fun showProgress(show: Boolean)
    fun showError(message: String?)
}