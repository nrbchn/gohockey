package com.gohockey.presentation.chat

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gohockey.model.FirestoreCollections
import com.gohockey.model.User
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot

@InjectViewState
class UsersPresenter : MvpPresenter<UsersView>() {

    private val currentUser = FirebaseAuth.getInstance().currentUser
    private val firestore = FirebaseFirestore.getInstance()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        refresh()
    }

    fun refresh() {
        if (currentUser == null) {
            //todo
        } else {
            viewState.showProgress(true)
            Tasks.whenAllSuccess<QuerySnapshot>(
                firestore.collection(FirestoreCollections.PLAYER).get(),
                firestore.collection(FirestoreCollections.KEEPER).get(),
                firestore.collection(FirestoreCollections.ADMIN).get()
            )
                .addOnSuccessListener {
                    val users = mutableListOf<User>()
                    it.forEach {
                        users.addAll(
                            it.documents.map {
                                User(
                                    it.get("userId") as String,
                                    it.get("name") as String + " " + it.get("surname") as String,
                                    it.get("avatarUrl") as String?
                                )
                            }
                        )
                    }

                    val data = users
                        .filter { it.id != currentUser.uid  }
                        .sortedBy { it.name }

                    viewState.showData(data)
                    if (data.isEmpty()) {
                        viewState.showError("Никого нет")
                    } else {
                        viewState.showError(null)
                    }
                }
                .addOnFailureListener {
                    viewState.showData(emptyList())
                    viewState.showError(it.message)
                }
                .addOnCompleteListener { viewState.showProgress(false) }
        }
    }
}