package com.gohockey.presentation.chat

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gohockey.model.User
import com.gohockey.model.message.Message
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

@InjectViewState
class ChatPresenter(
    private val user: User
) : MvpPresenter<ChatView>() {

    private val currentUser = FirebaseAuth.getInstance().currentUser
    private val database = FirebaseDatabase.getInstance()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.showUser(user)

        if (currentUser?.uid == null) {
            //todo
        } else {
            viewState.showProgress(true)
            database.reference
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val messages = mutableListOf<Message>()
                        val messageKeys = dataSnapshot
                            .child("user-messages")
                            .child(currentUser.uid)
                            .child(user.id)
                            .children
                            .map { it.key!! }

                        messageKeys.forEach { messageKey ->
                            val message = dataSnapshot
                                .child("messages")
                                .child(messageKey)
                                .getValue(Message::class.java)

                            messages.add(message!!)
                        }

                        messages.reverse()
                        Log.d("Chat messages", messages.toString())

                        val message = if (messages.isEmpty()) "Начните общение" else null
                        viewState.showProgress(false)
                        viewState.showError(message)
                        viewState.showMessages(messages)
                    }

                    override fun onCancelled(error: DatabaseError) {
                        Log.e("ChatPresenter","loadPost:onCancelled", error.toException())
                        viewState.showMessages(emptyList())
                        viewState.showProgress(false)
                        viewState.showError(error.message)
                    }
                })
        }
    }

    fun sendMessage(text: String) {
        if (text.isEmpty()) return

        if (currentUser?.uid == null) {
            //todo
        } else {
            val messageKey = database.reference
                .child("messages")
                .push()
                .key

            if (messageKey == null) {
                //todo
            } else {
                database.reference
                    .child("messages")
                    .child(messageKey)
                    .setValue(
                        Message(
                            currentUser.uid,
                            text,
                            Timestamp.now().seconds,
                            user.id
                        )
                    )

                database.reference
                    .child("user-messages")
                    .child(currentUser.uid)
                    .child(user.id)
                    .updateChildren(mapOf(messageKey to 1))

                database.reference
                    .child("user-messages")
                    .child(user.id)
                    .child(currentUser.uid)
                    .updateChildren(mapOf(messageKey to 1))
            }
        }
    }
}