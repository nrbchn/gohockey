package com.gohockey.presentation.chat

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.gohockey.model.User
import com.gohockey.model.message.Message

@StateStrategyType(AddToEndSingleStrategy::class)
interface ChatView : MvpView {
    fun showUser(user: User)
    fun showMessages(messages: List<Message>)
    fun showProgress(show: Boolean)
    fun showError(message: String?)
}