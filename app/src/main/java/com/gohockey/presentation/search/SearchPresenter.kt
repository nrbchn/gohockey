package com.gohockey.presentation.search

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gohockey.model.user.User
import com.gohockey.model.member.MemberRole
import com.gohockey.model.old.Metro
import com.gohockey.model.user.IdRole
import com.gohockey.repository.UserRepository
import com.gohockey.ui.map.Marker
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

@InjectViewState
class SearchPresenter(
    private val memberRole: MemberRole
) : MvpPresenter<SearchView>() {

    private val currentUser = FirebaseAuth.getInstance().currentUser
    private val firestore = FirebaseFirestore.getInstance()
    private val markers = mutableListOf<Marker>()

    private val userRepository = UserRepository(firestore)
    private var disposable: Disposable? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.showTitle(
            when (memberRole) {
                MemberRole.PLAYER -> "Игроки"
                MemberRole.KEEPER -> "Вратари"
                MemberRole.ADMIN -> "Администраторы"
            }
        )

        refresh()
    }

    fun refresh() {
        disposable?.dispose()
        disposable = userRepository.getUsers(
            when (memberRole) {
                MemberRole.PLAYER -> IdRole.PLAYER
                MemberRole.KEEPER -> IdRole.GOALKEEPER
                MemberRole.ADMIN -> IdRole.ADMIN_ICE
            }
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .map {
                it.filter {
                    currentUser != null && it.id != currentUser.uid && if (it is User.Goalkeeper) it.isVisible else true
                }
            }
            .subscribe(
                {
                    viewState.showData(it)
                    viewState.showError(if (it.isEmpty()) "Никого нет" else null)

                    markers.clear()
                    markers.addAll(
                        it.map {
                            val metro = Metro.getMetroById(it.metroId)
                            Marker(
                                lat = metro.lat,
                                lon = metro.lng,
                                user = com.gohockey.model.User(
                                    id = it.id,
                                    name = it.name + " " + it.surname,
                                    photo = it.avatarUrl
                                )
                            )
                        }
                    )
                },
                {
                    Log.e("SearchPresenter", it.message, it)
                    viewState.showData(emptyList())
                    viewState.showError(it.message)
                }
            )
    }

    fun openMap() {
        if (markers.isNotEmpty()) {
            viewState.showMap(markers)
        }
    }
}