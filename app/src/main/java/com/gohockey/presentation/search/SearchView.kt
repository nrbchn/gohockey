package com.gohockey.presentation.search

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.gohockey.ui.map.Marker

@StateStrategyType(AddToEndSingleStrategy::class)
interface SearchView : MvpView {
    fun showTitle(title: String)
    fun showData(data: List<Any>)
    fun showProgress(show: Boolean)
    fun showError(message: String?)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMap(markers: List<Marker>)
}