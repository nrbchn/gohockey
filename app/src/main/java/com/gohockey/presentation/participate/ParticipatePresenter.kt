package com.gohockey.presentation.participate

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gohockey.model.FirestoreCollections
import com.gohockey.model.Prefs
import com.gohockey.model.User
import com.gohockey.model.old.Metro
import com.gohockey.model.toIceOffer
import com.gohockey.presentation.main.feed.FeedPresenter
import com.gohockey.ui.main.feed.IceOfferItem
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

@InjectViewState
class ParticipatePresenter(
    private val prefs: Prefs,
    private val fusedLocationProviderClient: FusedLocationProviderClient
) : MvpPresenter<ParticipateView>() {

    private val currentUser = FirebaseAuth.getInstance().currentUser
    private val firestore = FirebaseFirestore.getInstance()
    private var disposable: Disposable? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        refresh()
    }

    fun refresh() {
        disposable?.dispose()
        disposable = getCurrentLocation()
            .subscribeOn(Schedulers.io())
            .flatMap { getIceOffers(it) }
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { iceOffers ->
                    val myOffers = iceOffers.filter {
                        currentUser?.uid != null && it.post.members.contains(currentUser.uid)
                    }
                    viewState.showError(if (myOffers.isEmpty()) "Вы нигде не участвуете" else null)
                    viewState.showData(myOffers.sortedByDescending { it.post.createdAt })
                },
                {
                    Log.e("ParticipatePresenter", "refresh error", it)
                    viewState.showData(emptyList())
                    viewState.showError("Не удалось загрузить")
                }
            )
    }

    private fun getCurrentLocation(): Single<LatLng> =
        Single.create<LatLng> { emitter ->
            try {
                fusedLocationProviderClient.lastLocation
                    .addOnSuccessListener { location ->
                        if (emitter.isDisposed) {
                            return@addOnSuccessListener
                        }

                        if (location != null) {
                            val latlng = LatLng(location.latitude, location.longitude)
                            prefs.lastLocation = latlng
                            emitter.onSuccess(latlng)
                        } else {
                            emitter.onSuccess(prefs.lastLocation)
                        }
                    }
                    .addOnFailureListener { }
            } catch (exception: SecurityException) {
                emitter.onSuccess(prefs.lastLocation)
            }
        }

    private fun getIceOffers(location: LatLng): Single<List<IceOfferItem>> =
        Single.create<List<IceOfferItem>> { emitter ->
            Tasks.whenAllSuccess<QuerySnapshot>(
                firestore.collection(FirestoreCollections.ICE_OFFERS).get(),
                firestore.collection(FirestoreCollections.PLAYER).get(),
                firestore.collection(FirestoreCollections.ADMIN).get(),
                firestore.collection(FirestoreCollections.KEEPER).get()
            )
                .addOnSuccessListener { snapshots ->
                    if (emitter.isDisposed) {
                        return@addOnSuccessListener
                    }

                    val iceOffers = mutableListOf<IceOfferItem>()
                    snapshots[0].forEach { document ->
                        try {
                            val post = document.data.toIceOffer()
                            val metro = Metro.getMetroById(post.metroId)

                            val userSnapshot =
                                snapshots[1].documents.find { it.data?.get("userId") == post.userId } ?:
                                snapshots[2].documents.find { it.data?.get("userId") == post.userId } ?:
                                snapshots[3].documents.find { it.data?.get("userId") == post.userId }

                            userSnapshot?.let {
                                val user = User(
                                    id = userSnapshot.get("userId").toString(),
                                    name = userSnapshot.get("name").toString() + " " + userSnapshot.get("surname").toString(),
                                    photo = userSnapshot.get("avatarUrl") as? String
                                )
                                val dist = FeedPresenter.distance(
                                    location.latitude, location.longitude,
                                    metro.lat, metro.lng
                                )
                                iceOffers.add(IceOfferItem(user, post, dist))
                            }
                        } catch (e: Exception) {
                            Log.e("ParticipatePresenter", e.message, e)
                        }
                    }

                    emitter.onSuccess(iceOffers)
                }
                .addOnFailureListener { emitter.onError(it) }
        }

    override fun onDestroy() {
        disposable?.dispose()
        super.onDestroy()
    }
}