package com.gohockey.presentation.myposts

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface MyPostsView : MvpView {
    fun showData(data: List<Any>)
    fun showProgress(show: Boolean)
    fun showError(message: String?)
}