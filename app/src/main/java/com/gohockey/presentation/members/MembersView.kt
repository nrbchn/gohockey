package com.gohockey.presentation.members

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface MembersView : MvpView {
    fun showTitle(title: String)
    fun showData(data: List<Any>)
    fun showProgress(show: Boolean)
    fun showError(message: String?)
}