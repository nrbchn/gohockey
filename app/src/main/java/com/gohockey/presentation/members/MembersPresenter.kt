package com.gohockey.presentation.members

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.gohockey.model.FirestoreCollections
import com.gohockey.model.IceOffer
import com.gohockey.model.User
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot

@InjectViewState
class MembersPresenter(
    private val iceOffer: IceOffer
) : MvpPresenter<MembersView>() {

    private val currentUser = FirebaseAuth.getInstance().currentUser
    private val firestore = FirebaseFirestore.getInstance()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.showTitle("Участники")

        refresh()
    }

    fun refresh() {
        if (currentUser == null) {
            //todo
        } else {
            viewState.showProgress(true)

            Tasks.whenAllSuccess<QuerySnapshot>(
                firestore.collection(FirestoreCollections.PLAYER).get(),
                firestore.collection(FirestoreCollections.KEEPER).get(),
                firestore.collection(FirestoreCollections.ADMIN).get()
            )
                .addOnSuccessListener { snapshots ->
                    val users = mutableListOf<User>()

                    iceOffer.members.forEach { memberId ->
                        snapshots.forEach { snapshot ->
                            snapshot.documents
                                .filter { it["userId"] == memberId }
                                .map {
                                    User(
                                        it.get("userId") as String,
                                        it.get("name") as String + " " + it.get("surname") as String,
                                        it.get("avatarUrl") as String?
                                    )
                                }
                                .forEach { user -> users.add(user) }
                        }
                    }

                    users.sortBy { it.name }
                    viewState.showData(users)
                    if (users.isEmpty()) {
                        viewState.showError("Никого нет")
                    } else {
                        viewState.showError(null)
                    }
                }
                .addOnFailureListener {
                    viewState.showData(emptyList())
                    viewState.showError(it.message)
                }
                .addOnCompleteListener { viewState.showProgress(false) }
        }
    }
}