package com.gohockey.model.old

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GoalKeeper(
        override val userId: String = "",
        val teamId: String? = null,
        val age: String = "",
        val handSide: Int = -1,
        val height: String = "",
        val idRole: String = "",
        override val metroId: String = "",
        override val name: String = "",
        val qualification: String = "",
        override val surname: String = "",
        val weight: String = "",
        override val avatarUrl: String = "",
        val priceFrom: String = "",
        val priceTo: String = "",
        val isVisible: Boolean = false
) : CommonUser(userId, name, surname, avatarUrl, metroId), Parcelable
