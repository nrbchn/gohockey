package com.gohockey.model.old

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Referee(
        override val userId: String = "",
        val idRole: String = "",
        override val name: String = "",
        override val surname: String = "",
        override val avatarUrl: String = "",
        val priceGameFrom: String = "",
        val priceGameTo: String = "",
        val additaionInformation: String = "",
        override val metroId: String = "",
        val visible: Boolean = false,
        val accreditation: Boolean = false
) : CommonUser(userId, name, surname, avatarUrl, metroId), Parcelable
