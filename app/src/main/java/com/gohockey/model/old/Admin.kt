package com.gohockey.model.old

import android.os.Parcelable
import com.gohockey.model.old.CommonUser
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Admin(
        override val userId: String="",
        val idRole: String = "",
        val work: String = "",
        val teamId: String? = null,
        val telephone: String = "",
        override val name: String = "",
        override val surname: String = "",
        override val avatarUrl: String = "",
        val additaionInformation: String = "",
        override val metroId: String = ""
) : CommonUser(userId, name, surname, avatarUrl, metroId), Parcelable
