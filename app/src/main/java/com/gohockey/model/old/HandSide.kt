package com.gohockey.model.old

enum class HandSide(val id: Int, val handSideName: String) {
    LEFT(0, "Левый"),
    RIGHT(1, "Правый"),
    BOTH(2, "Оба");

    companion object {
        fun getHandSideById(id: Int) = values().first { it.id == id }.handSideName
    }

}
