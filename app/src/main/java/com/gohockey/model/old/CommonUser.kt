package com.gohockey.model.old

import java.util.Date

open class CommonUser(open val userId: String, open val name: String, open val surname: String, open val avatarUrl: String, open val metroId: String, val createdAt: Date = Date()) {

    constructor() : this("", "", "", "", "")

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is CommonUser) return false

        if (userId != other.userId) return false
        if (name != other.name) return false
        if (surname != other.surname) return false
        if (avatarUrl != other.avatarUrl) return false
        if (metroId != other.metroId) return false
        if (createdAt != other.createdAt) return false

        return true
    }

    override fun hashCode(): Int {
        var result = userId.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + surname.hashCode()
        result = 31 * result + avatarUrl.hashCode()
        result = 31 * result + metroId.hashCode()
        result = 31 * result + createdAt.hashCode()
        return result
    }

}
