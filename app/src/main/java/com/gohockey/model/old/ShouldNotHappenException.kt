package com.gohockey.model.old

class ShouldNotHappenException : RuntimeException {

    constructor() : super() {}

    constructor(detailMessage: String) : super(detailMessage) {}

    constructor(detailMessage: String, throwable: Throwable) : super(detailMessage, throwable) {}

    constructor(throwable: Throwable) : super(throwable) {}

    companion object {

        private val serialVersionUID: Long = 0
    }

}
