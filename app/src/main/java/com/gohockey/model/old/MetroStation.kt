package com.gohockey.model.old

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MetroStation(val id: String, val name: String, val lat: Double, val lng: Double, val order: Int) : Parcelable
