package com.gohockey.model.old

import android.os.Parcelable
import com.gohockey.model.old.CommonUser
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Coach(
        override val userId: String = "",
        val teamId: String? = null,
        val experience: String = "",
        val idRole: String = "",
        override val metroId: String = "",
        override val name: String = "",
        override val surname: String = "",
        override val avatarUrl: String = "",
        val priceGameFrom: String = "",
        val priceGameTo: String = "",
        val priceTrainFrom: String = "",
        val priceTrainTo: String = "",
        val visible: Boolean = false
) : CommonUser(userId, name, surname, avatarUrl, metroId), Parcelable
