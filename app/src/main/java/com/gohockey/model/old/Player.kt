package com.gohockey.model.old

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Player(
        override val userId: String = "",
        val teamId: String? = null,
        val age: String = "",
        val find: String = "",
        val handSide: Int = -1,
        val height: String = "",
        val idRole: String = "",
        override val metroId: String = "",
        override val name: String = "",
        val qualification: String = "",
        val role: String = "",
        override val surname: String = "",
        val weight: String = "",
        override val avatarUrl: String = ""
) : CommonUser(userId, name, surname, avatarUrl, metroId), Parcelable
