package com.gohockey.model.message

import com.gohockey.model.User

data class MessageItem(
    val user: User,
    val text: String,
    val date: String? = null
)