package com.gohockey.model.message

import com.google.firebase.Timestamp
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class Message(
    val fromId: String = "",
    val text: String = "",
    val timestamp: Long = Timestamp.now().seconds,
    val toId: String = ""
)