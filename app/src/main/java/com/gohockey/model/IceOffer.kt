package com.gohockey.model

import android.os.Parcelable
import com.google.firebase.Timestamp
import kotlinx.android.parcel.Parcelize

@Parcelize
data class IceOffer(
    val avatarUrl: String,
    val coach: Boolean,
    val createdAt: Timestamp,
    val endDate: Timestamp,
    val goalKeeper: Boolean,
    val id: String,
    val members: List<String>,
    val membersWait: List<String>,
    val metroId: String,
    val nameIce: String,
    val price: String,
    val qualification: String,
    val startDate: Timestamp,
    val theme: String,
    val userId: String
) : Parcelable

fun Map<String, Any>.toIceOffer() = IceOffer(
    get("avatarUrl") as String,
    get("coach") as Boolean,
    get("createdAt") as Timestamp,
    get("endDate") as Timestamp,
    get("goalKeeper") as Boolean,
    get("id") as String,
    (get("members") as ArrayList<String>).toList(),
    (get("membersWait") as ArrayList<String>).toList(),
    get("metroId") as String,
    get("nameIce") as String,
    get("price") as String,
    get("qualification") as String,
    get("startDate") as Timestamp,
    get("theme") as String,
    get("userId") as String
)