package com.gohockey.model.subway

data class SubwayCity(
    val id: Int,
    val name: String,
    val lines: List<SubwayLine>
)