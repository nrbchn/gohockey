package com.gohockey.model.subway

data class SubwayLine(
    val id: Int,
    val hex_color: String,
    val name: String,
    val stations: List<SubwayStation>
)