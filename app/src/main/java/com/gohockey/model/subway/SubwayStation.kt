package com.gohockey.model.subway

data class SubwayStation(
    val id: String,
    val name: String,
    val lat: Double,
    val lng: Double,
    val order: Int
)