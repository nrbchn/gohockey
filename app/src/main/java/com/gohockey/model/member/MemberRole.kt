package com.gohockey.model.member

import com.gohockey.R
import com.google.gson.annotations.SerializedName

enum class MemberRole(
    val desc: String,
    val iconResId: Int
) {
    @SerializedName("Игрок") PLAYER("Игрок", R.drawable.ic_player),
    @SerializedName("Вратарь") KEEPER("Вратарь", R.drawable.ic_keeper),
    @SerializedName("Администратор") ADMIN("Администратор", R.drawable.ic_admin)
}