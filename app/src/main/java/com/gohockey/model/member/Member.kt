package com.gohockey.model.member

import com.google.gson.annotations.SerializedName

data class Member(
    @SerializedName("email") val email: String,
    @SerializedName("id") val id: Int,
    @SerializedName("idRole") val role: MemberRole,
    @SerializedName("telephone") val phone: String? = null
)