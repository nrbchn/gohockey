package com.gohockey.model

import android.content.Context
import com.gohockey.ui.map.DEFAULT_LATLNG
import com.google.android.gms.maps.model.LatLng

class Prefs(
    private val context: Context
) {
    private val APP_DATA = "app data"
    private val AUTH_DATA = "auth data"
    private val KEY_LOCATION_LATITUDE = "my loc lat"
    private val KEY_LOCATION_LONGITUDE = "my loc lon"
    private val KEY_MESSAGES_COUNT = "messages count"
    private val KEY_REQUESTS_COUNT = "requests count"

    var lastLocation: LatLng
        get() = LatLng(
            getSharedPreferences(APP_DATA).getString(KEY_LOCATION_LATITUDE, null)?.toDoubleOrNull() ?: DEFAULT_LATLNG.latitude,
            getSharedPreferences(APP_DATA).getString(KEY_LOCATION_LONGITUDE, null)?.toDoubleOrNull() ?: DEFAULT_LATLNG.longitude
        )
        set(value) {
            getSharedPreferences(APP_DATA).edit().putString(KEY_LOCATION_LATITUDE, value.latitude.toString()).apply()
            getSharedPreferences(APP_DATA).edit().putString(KEY_LOCATION_LONGITUDE, value.longitude.toString()).apply()
        }

    var messages: Int
        get() = getSharedPreferences(AUTH_DATA).getInt(KEY_MESSAGES_COUNT, 0)
        set(value) {
            getSharedPreferences(AUTH_DATA).edit().putInt(KEY_MESSAGES_COUNT, value).apply()
        }

    var requests: Int
        get() = getSharedPreferences(AUTH_DATA).getInt(KEY_REQUESTS_COUNT, 0)
        set(value) {
            getSharedPreferences(AUTH_DATA).edit().putInt(KEY_REQUESTS_COUNT, value).apply()
        }

    private fun getSharedPreferences(prefsName: String) =
        context.getSharedPreferences(prefsName, Context.MODE_PRIVATE)
}