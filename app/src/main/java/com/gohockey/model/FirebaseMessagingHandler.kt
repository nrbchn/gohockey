package com.gohockey.model

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.gohockey.App
import com.gohockey.R
import com.gohockey.ui.main.MainActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.util.Random

class FirebaseMessagingHandler : FirebaseMessagingService() {

    private val prefs by lazy { Prefs(this) }
    private val notificationManager by lazy {
        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d("FirebaseMessaging", remoteMessage.data.toString())

        val notificationId = Random(1000).nextInt()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setupChannels(remoteMessage.data["type"].orEmpty())
        }

        if (remoteMessage.data["type"] == "chat") {
            App.notificationController.setMessages(prefs.messages + 1)
            prefs.messages++
        } else if (remoteMessage.data["type"] == "membersWait") {
            App.notificationController.setRequests(prefs.requests + 1)
            prefs.requests++
        }

        val intent = Intent(applicationContext, MainActivity::class.java)

        val notificationBuilder = NotificationCompat.Builder(this, remoteMessage.data["type"].orEmpty())
            .setSmallIcon(R.drawable.vector_player_white_120x77)
            .setContentTitle(remoteMessage.data["title"])
            .setContentText(remoteMessage.data["body"]) //ditto
            .setColor(ContextCompat.getColor(this, R.color.colorAccent))
            .setAutoCancel(true)
            .setContentIntent(PendingIntent.getActivity(applicationContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT))
            .setNumber(prefs.messages + prefs.requests)

        notificationManager.notify(notificationId, notificationBuilder.build())
    }

    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        Log.d("TOKEN_APP", "Refreshed token: $token")
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupChannels(channel: String) {
        val adminChannelName = "Sound"
        val adminChannelDescription = "Sound when impact received"
        val adminChannel = NotificationChannel(channel, adminChannelName, NotificationManager.IMPORTANCE_HIGH)
        adminChannel.description = adminChannelDescription
        adminChannel.enableLights(true)
        adminChannel.lightColor = Color.RED
        adminChannel.enableVibration(true)
        adminChannel.setShowBadge(true)
        notificationManager.createNotificationChannel(adminChannel)
    }
}
