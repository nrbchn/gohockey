package com.gohockey.model

import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable

class NotificationController(
    private val prefs: Prefs
) {

    private val messagesRelay = BehaviorRelay.createDefault(prefs.messages)
    private val requestsRelay = BehaviorRelay.createDefault(prefs.requests)

    val messagesState: Observable<Int> = messagesRelay
    val requestsState: Observable<Int> = requestsRelay

    fun setMessages(count: Int) {
        //prefs.messages = count
        messagesRelay.accept(count)
    }
    fun setRequests(count: Int) {
        //prefs.requests = count
        requestsRelay.accept(count)
    }
}