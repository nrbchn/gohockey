package com.gohockey.model.user

import com.gohockey.R
import com.google.gson.annotations.SerializedName

enum class IdRole(
    val title: String,
    val iconResId: Int
) {
    @SerializedName("Игрок") PLAYER("Игрок", R.drawable.ic_player),
    @SerializedName("Вратарь") GOALKEEPER("Вратарь", R.drawable.ic_keeper),
    @SerializedName("Администратор") ADMIN_ICE("Администратор", R.drawable.ic_admin);
}