package com.gohockey.model.user

enum class HandSide(
    val id: Int,
    val title: String
) {
    UNDEFINED(-1, "Не указан"),
    RIGHT(0, "Правый"),
    LEFT(1, "Левый"),
    BOTH(2, "Правый и левый");

    companion object {
        fun fromId(id: Int) = when (id) {
            0 -> RIGHT
            1 -> LEFT
            2 -> BOTH
            else -> UNDEFINED
        }
    }
}