package com.gohockey.model.user

import android.os.Parcelable
import com.google.firebase.Timestamp
import kotlinx.android.parcel.Parcelize
import java.lang.NumberFormatException
import java.util.*
import kotlin.collections.HashMap

sealed class User(
    open val id: String,
    open val avatarUrl: String,
    open val name: String,
    open val surname: String,
    open val createdAt: Date,
    open val idRole: IdRole,
    open val metroId: String
) : Parcelable {

    abstract fun hashMap(): HashMap<String, Any?>

    @Parcelize
    data class Player(
        override val id: String,
        override val avatarUrl: String,
        override val name: String,
        override val surname: String,
        override val createdAt: Date,
        override val metroId: String,
        val age: Int,
        val handSide: HandSide,
        val height: Int,
        val weight: Int,
        val level: String,
        val line: String
    ) : User(id, avatarUrl, name, surname, createdAt, IdRole.PLAYER, metroId) {

        override fun hashMap(): HashMap<String, Any?> = hashMapOf(
            "userId" to id,
            "avatarUrl" to avatarUrl,
            "name" to name,
            "surname" to surname,
            "createdAt" to createdAt,
            "idRole" to idRole.title,
            "metroId" to metroId,
            "age" to age,
            "handSide" to handSide.id,
            "height" to height,
            "weight" to weight,
            "qualification" to level,
            "role" to line
        )

        companion object {
            fun fromMap(map: Map<String, *>) = Player(
                id = map["userId"] as String,
                avatarUrl = map["avatarUrl"] as String,
                name = map["name"] as String,
                surname = map["surname"] as String,
                createdAt = (map["createdAt"] as Timestamp).toDate(),
                metroId = map["metroId"] as String,
                age = try { (map["age"] as String).toInt() } catch (e: NumberFormatException) { 0 },
                handSide = HandSide.fromId((map["handSide"] as Long).toInt()),
                height = try { (map["height"] as String).toInt() } catch (e: NumberFormatException) { 0 },
                weight = try { (map["weight"] as String).toInt() } catch (e: NumberFormatException) { 0 },
                level = map["qualification"] as String,
                line = map["role"] as String
            )
        }
    }

    @Parcelize
    data class Goalkeeper(
        override val id: String,
        override val avatarUrl: String,
        override val name: String,
        override val surname: String,
        override val createdAt: Date,
        override val metroId: String,
        val age: Int,
        val handSide: HandSide,
        val height: Int,
        val weight: Int,
        val level: String,
        val priceFrom: Int,
        val priceTo: Int,
        val isVisible: Boolean
    ) : User(id, avatarUrl, name, surname, createdAt, IdRole.GOALKEEPER, metroId) {

        override fun hashMap(): HashMap<String, Any?> = hashMapOf(
            "userId" to id,
            "avatarUrl" to avatarUrl,
            "name" to name,
            "surname" to surname,
            "createdAt" to createdAt,
            "idRole" to idRole.title,
            "metroId" to metroId,
            "age" to age,
            "handSide" to handSide.id,
            "height" to height,
            "weight" to weight,
            "qualification" to level,
            "priceFrom" to priceFrom,
            "priceTo" to priceTo,
            "isVisible" to isVisible
        )

        companion object {
            fun fromMap(map: Map<String, *>) = Goalkeeper(
                id = map["userId"] as String,
                avatarUrl = map["avatarUrl"] as String,
                name = map["name"] as String,
                surname = map["surname"] as String,
                createdAt = (map["createdAt"] as Timestamp).toDate(),
                metroId = map["metroId"] as String,
                age = try { (map["age"] as String).toInt() } catch (e: NumberFormatException) { 0 },
                handSide = HandSide.fromId((map["handSide"] as Long).toInt()),
                height = try { (map["height"] as String).toInt() } catch (e: NumberFormatException) { 0 },
                weight = try { (map["weight"] as String).toInt() } catch (e: NumberFormatException) { 0 },
                level = map["qualification"] as String,
                priceFrom = try { (map["priceFrom"] as String).toInt() } catch (e: NumberFormatException) { 0 },
                priceTo = try { (map["priceTo"] as String).toInt() } catch (e: NumberFormatException) { 0 },
                isVisible = map["isVisible"] as Boolean
            )
        }
    }

    @Parcelize
    data class AdminIce(
        override val id: String,
        override val avatarUrl: String,
        override val name: String,
        override val surname: String,
        override val createdAt: Date,
        override val metroId: String,
        val teamId: String?,
        val telephone: String,
        val work: String,
        val about: String
    ) : User(id, avatarUrl, name, surname, createdAt, IdRole.ADMIN_ICE, metroId) {

        override fun hashMap(): HashMap<String, Any?> = hashMapOf(
            "userId" to id,
            "avatarUrl" to avatarUrl,
            "name" to name,
            "surname" to surname,
            "createdAt" to createdAt,
            "idRole" to idRole.title,
            "metroId" to metroId,
            "teamId" to teamId,
            "telephone" to telephone,
            "work" to work,
            "additaionInformation" to about
        )

        companion object {
            fun fromMap(map: Map<String, *>) = AdminIce(
                id = map["userId"] as String,
                avatarUrl = map["avatarUrl"] as String,
                name = map["name"] as String,
                surname = map["surname"] as String,
                createdAt = (map["createdAt"] as Timestamp).toDate(),
                metroId = map["metroId"] as String,
                teamId = map["teamId"] as? String,
                telephone = map["telephone"] as String,
                work = map["work"] as String,
                about = map["additaionInformation"] as String
            )
        }
    }
}