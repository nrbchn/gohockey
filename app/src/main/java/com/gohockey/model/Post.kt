package com.gohockey.model

data class Post(
    val user: User,
    val image: String,
    val likes: Int,
    val title: String,
    val desc: String
)