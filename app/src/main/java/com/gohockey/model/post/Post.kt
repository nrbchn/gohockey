package com.gohockey.model.post

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.collections.ArrayList

sealed class Post : Parcelable {

    abstract fun hashMap(): HashMap<String, Any?>

    @Parcelize
    data class IceOffer(
        val id: String,
        val metroId: String,
        val createdAt: Date,
        val startDate: Date,
        val endDate: Date,
        val userId: String,
        val contactName: String,
        val address: String,
        val goalKeeper: Boolean,
        val coach: Boolean,
        val members: List<String>,
        val membersWait: List<String>,
        val nameIce: String,
        val price: String,
        val level: String,
        val theme: String
    ) : Post() {

        override fun hashMap(): HashMap<String, Any?> = hashMapOf(
            "id" to id,
            "metroId" to metroId,
            "createdAt" to createdAt,
            "startDate" to startDate,
            "endDate" to endDate,
            "userId" to userId,
            "contactName" to contactName,
            "address" to address,
            "goalKeeper" to goalKeeper,
            "coach" to coach,
            "members" to members,
            "membersWait" to membersWait,
            "nameIce" to nameIce,
            "price" to price,
            "qualification" to level,
            "theme" to theme
        )

        companion object {
            fun fromMap(map: Map<String, *>) = IceOffer(
                id = map["id"] as String,
                metroId = map["metroId"] as String,
                createdAt = map["createdDate"] as Date,
                startDate = map["startDate"] as Date,
                endDate = map["endDate"] as Date,
                userId = map["userId"] as String,
                contactName = (map["contactName"] as? String) ?: "",
                address = map["address"] as String,
                goalKeeper = map["goalKeeper"] as Boolean,
                coach = map["coach"] as Boolean,
                members = (map["members"] as ArrayList<String>).toList(),
                membersWait = (map["membersWait"] as ArrayList<String>).toList(),
                nameIce = map["nameIce"] as String,
                price = map["price"] as String,
                level = map["qualification"] as String,
                theme = map["theme"] as String
            )
        }
    }
}