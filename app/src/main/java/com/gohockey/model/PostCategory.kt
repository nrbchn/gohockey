package com.gohockey.model

import com.gohockey.R

sealed class PostCategory(
    val name: String,
    val resId: Int
) {
    class Team : PostCategory("Команда", R.drawable.ic_team)
    class Ice : PostCategory("Лед", R.drawable.ic_ice)
    class Match : PostCategory("Товарищеский матч", R.drawable.ic_match)
    class Tackle : PostCategory("Подкатка", R.drawable.ic_tackle)
    class Cup : PostCategory("Турнир", R.drawable.ic_cup)
    class Rink : PostCategory("Ледовый дворец/каток", R.drawable.ic_rink)
}