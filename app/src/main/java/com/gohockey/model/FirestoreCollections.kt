package com.gohockey.model

object FirestoreCollections {
    const val ICE_OFFERS = "IceOffers"
    const val USERS = "Users"
    const val PLAYER = "Player"
    const val KEEPER = "GoalKeeper"
    const val ADMIN = "AdminIce"
}