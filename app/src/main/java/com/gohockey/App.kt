package com.gohockey

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDex
import com.gohockey.model.NotificationController
import com.gohockey.model.Prefs
import com.gohockey.model.subway.SubwayCity
import com.gohockey.model.subway.SubwayStation
import com.gohockey.ui.map.DEFAULT_LATLNG
import com.google.gson.Gson
import ru.terrakok.cicerone.Cicerone
import java.util.*

class App : Application() {
    companion object {
        lateinit var appCode: String
            private set

        private val cicerone = Cicerone.create()
        val navigatorHolder get() = cicerone.navigatorHolder
        val router get() = cicerone.router

        lateinit var subways: List<SubwayStation>
            private set

        var currentLocation = DEFAULT_LATLNG

        lateinit var notificationController: NotificationController
            private set
    }

    override fun onCreate() {
        super.onCreate()
        appCode = UUID.randomUUID().toString()

        val subwayCity = Gson().fromJson(resources.openRawResource(R.raw.subways).bufferedReader().use { it.readText() }, SubwayCity::class.java)
        val subwayStations = mutableListOf<SubwayStation>().apply {
            subwayCity.lines.forEach {
                it.stations.forEach {
                    this.add(it)
                }
            }
        }

        subways = subwayStations.sortedBy { it.name }

        notificationController = NotificationController(Prefs(this))

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}