package com.gohockey.ui.chat

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.arellomobile.mvp.MvpAppCompatActivity
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.User
import com.gohockey.ui.global.BaseFragment
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.Command

class ChatActivity : MvpAppCompatActivity() {

    private val user: User? get() = intent.extras?.getParcelable(ARG_USER)

    private val currentFragment: BaseFragment?
        get() = supportFragmentManager.findFragmentById(R.id.content_layout) as? BaseFragment

    private val navigator: Navigator =
        object : SupportAppNavigator(this, supportFragmentManager, R.id.content_layout) {
            override fun setupFragmentTransaction(
                command: Command?,
                currentFragment: Fragment?,
                nextFragment: Fragment?,
                fragmentTransaction: FragmentTransaction
            ) {
                fragmentTransaction.setReorderingAllowed(true)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_container)

        if (savedInstanceState == null) {
            App.router.newRootScreen(
                if (user == null) {
                    Screens.Users
                } else {
                    Screens.Chat(user!!)
                }
            )
        }
    }

    override fun onResume() {
        super.onResume()
        App.navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        App.navigatorHolder.removeNavigator()
    }

    override fun onBackPressed() {
        currentFragment?.onBackPressed() ?: super.onBackPressed()
    }

    companion object {
        private const val ARG_USER = "chat user"

        fun getIntent(context: Context, user: User?) = Intent(context, ChatActivity::class.java).apply {
            putExtra(ARG_USER, user)
        }
    }
}

object Screens {
    object Users : SupportAppScreen() {
        override fun getFragment() = UsersFragment()
    }
    data class Chat(
        private val user: User
    ) : SupportAppScreen() {
        override fun getFragment() = ChatFragment.newInstance(user)
    }
}