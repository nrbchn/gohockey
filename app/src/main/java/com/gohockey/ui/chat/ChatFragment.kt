package com.gohockey.ui.chat

import android.graphics.Rect
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import androidx.core.view.get
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.bumptech.glide.Glide
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.User
import com.gohockey.model.message.Message
import com.gohockey.presentation.chat.ChatPresenter
import com.gohockey.presentation.chat.ChatView
import com.gohockey.ui.global.BaseFragment
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import kotlinx.android.synthetic.main.fragment_chat.*

class ChatFragment : BaseFragment(), ChatView {

    override val layoutRes = R.layout.fragment_chat

    private val user: User get() = arguments!!.getParcelable(ARG_USER)!!
    private val adapter by lazy { ChatAdapter() }

    @InjectPresenter
    lateinit var presenter: ChatPresenter

    @ProvidePresenter
    fun providePresenter() = ChatPresenter(user)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { App.router.exit() }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, true)
            adapter = this@ChatFragment.adapter
            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(
                    outRect: Rect,
                    view: View,
                    parent: RecyclerView,
                    state: RecyclerView.State
                ) {
                    if (parent.getChildAdapterPosition(view) == 0) {
                        outRect.bottom = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16f, resources.displayMetrics).toInt()
                    }
                    outRect.left = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16f, resources.displayMetrics).toInt()
                    outRect.right = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16f, resources.displayMetrics).toInt()
                    outRect.top = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16f, resources.displayMetrics).toInt()
                }
            })
        }
        chat_send_button.setOnClickListener {
            presenter.sendMessage(chat_edit_text.text.toString().trim())
            chat_edit_text.text.clear()
        }
    }

    override fun showUser(user: User) {
        chat_user_name_text_view.text = user.name
        Glide.with(this)
            .load(user.photo)
            .apply {
                placeholder(R.drawable.ic_user_placeholder)
                error(R.drawable.ic_user_placeholder)
                circleCrop()
            }
            .into(chat_user_image_view)
    }

    override fun showMessages(messages: List<Message>) {
        adapter.setData(messages)
    }

    override fun showProgress(show: Boolean) {
        progress_bar.isVisible = show
    }

    override fun showError(message: String?) {
        error_text_view.isVisible = !message.isNullOrEmpty()
        error_text_view.text = message
    }

    override fun onBackPressed() {
        App.router.exit()
    }

    private inner class ChatAdapter : ListDelegationAdapter<MutableList<Any>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(MessageAdapterDelegate(user.id))
        }

        fun setData(data: List<Any>) {
            items.clear()
            items.addAll(data)
            notifyDataSetChanged()
        }
    }

    companion object {
        private const val ARG_USER = "chat user"

        fun newInstance(user: User) = ChatFragment().apply {
            arguments = Bundle().apply {
                putParcelable(ARG_USER, user)
            }
        }
    }
}