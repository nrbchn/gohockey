package com.gohockey.ui.chat

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.gohockey.App
import com.gohockey.R
import com.gohockey.presentation.chat.UsersPresenter
import com.gohockey.presentation.chat.UsersView
import com.gohockey.ui.global.BaseFragment
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import kotlinx.android.synthetic.main.fragment_chat_users.*

class UsersFragment : BaseFragment(), UsersView {

    override val layoutRes = R.layout.fragment_chat_users

    private val adapter by lazy { UsersAdapter() }

    @InjectPresenter
    lateinit var presenter: UsersPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { App.router.exit() }
        swipe_refresh.setOnRefreshListener { presenter.refresh() }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(context)
            adapter = this@UsersFragment.adapter
        }
    }

    override fun showData(data: List<Any>) {
        postViewAction { adapter.setData(data) }
    }

    override fun showError(message: String?) {
        error_text_view.text = message
        error_text_view.isVisible = !message.isNullOrEmpty()
    }

    override fun showProgress(show: Boolean) {
        swipe_refresh.isRefreshing = show
    }

    override fun onBackPressed() {
        App.router.exit()
    }

    private inner class UsersAdapter : ListDelegationAdapter<MutableList<Any>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(UserAdapterDelegate { App.router.newRootScreen(Screens.Chat(it)) })
        }

        fun setData(data: List<Any>) {
            items.clear()
            items.addAll(data)
            notifyDataSetChanged()
        }
    }
}