package com.gohockey.ui.user

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.arellomobile.mvp.MvpAppCompatActivity
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.IceOffer
import com.gohockey.ui.global.BaseFragment
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.Command

class UserActivity : MvpAppCompatActivity() {

    private val userId get() = intent.extras!!.getString(ARG_USER_ID)!!
    private val requestOffer get() = intent.extras?.getParcelable<IceOffer>(ARG_ICE_OFFER)

    private val currentFragment: BaseFragment?
        get() = supportFragmentManager.findFragmentById(R.id.content_layout) as? BaseFragment

    private val navigator: Navigator =
        object : SupportAppNavigator(this, supportFragmentManager, R.id.content_layout) {
            override fun setupFragmentTransaction(
                command: Command?,
                currentFragment: Fragment?,
                nextFragment: Fragment?,
                fragmentTransaction: FragmentTransaction
            ) {
                fragmentTransaction.setReorderingAllowed(true)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_container)
    }

    override fun onResume() {
        super.onResume()
        App.navigatorHolder.setNavigator(navigator)
        App.router.newRootScreen(Screens.Loading(userId, requestOffer))
    }

    override fun onPause() {
        super.onPause()
        App.navigatorHolder.removeNavigator()
    }

    override fun onBackPressed() {
        currentFragment?.onBackPressed() ?: super.onBackPressed()
    }

    companion object {
        private const val ARG_USER_ID = "user id"
        private const val ARG_ICE_OFFER = "ice offer request"

        fun getIntent(context: Context, userId: String, requestOffer: IceOffer? = null) = Intent(context, UserActivity::class.java).apply {
            putExtra(ARG_USER_ID, userId)
            putExtra(ARG_ICE_OFFER, requestOffer)
        }
    }
}

object Screens {
    data class Loading(
        private val userId: String,
        private val iceOffer: IceOffer?
    ) : SupportAppScreen() {
        override fun getFragment() = LoadingFragment.newInstance(userId, iceOffer)
    }
    data class Player(
        private val player: com.gohockey.model.old.Player,
        private val iceOffer: IceOffer?
    ) : SupportAppScreen() {
        override fun getFragment() = PlayerFragment.newInstance(player, iceOffer)
    }
    data class GoalKeeper(
        private val keeper: com.gohockey.model.old.GoalKeeper,
        private val iceOffer: IceOffer?
    ) : SupportAppScreen() {
        override fun getFragment() = GoalKeeperFragment.newInstance(keeper, iceOffer)
    }
    data class Admin(
        private val admin: com.gohockey.model.old.Admin,
        private val iceOffer: IceOffer?
    ) : SupportAppScreen() {
        override fun getFragment() = AdminFragment.newInstance(admin, iceOffer)
    }
}