package com.gohockey.ui.user

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.IceOffer
import com.gohockey.model.User
import com.gohockey.model.old.GoalKeeper
import com.gohockey.model.old.HandSide
import com.gohockey.ui.chat.ChatActivity
import com.gohockey.ui.edit.profile.EditActivity
import com.gohockey.ui.global.BaseFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_goalkeeper.*
import kotlinx.android.synthetic.main.fragment_goalkeeper.accept_button
import kotlinx.android.synthetic.main.fragment_goalkeeper.chat_button
import kotlinx.android.synthetic.main.fragment_goalkeeper.edit_button
import kotlinx.android.synthetic.main.fragment_goalkeeper.reject_button
import kotlinx.android.synthetic.main.fragment_player.*

class GoalKeeperFragment : BaseFragment() {

    companion object {

        const val REQUEST_CODE = 998

        private const val STATE = "STATE"
        private const val REQUEST_OFFER = "request ice offer"

        fun newInstance(goalKeeper: GoalKeeper, iceOffer: IceOffer? = null): GoalKeeperFragment {
            val fragment = GoalKeeperFragment()
            fragment.arguments = Bundle().apply {
                putParcelable(STATE, goalKeeper)
                putParcelable(REQUEST_OFFER, iceOffer)
            }
            return fragment
        }

    }

    override val layoutRes = R.layout.fragment_goalkeeper

    private val currentUser = FirebaseAuth.getInstance()
    private val firestore = FirebaseFirestore.getInstance()

    private val state: GoalKeeper by lazy { arguments?.getParcelable(STATE) as GoalKeeper }
    private val iceOffer: IceOffer? get() = arguments?.getParcelable(REQUEST_OFFER)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(this)
            .load(state.avatarUrl)
            .apply {
                placeholder(R.drawable.ic_user_placeholder_60)
                error(R.drawable.ic_user_placeholder_60)
                circleCrop()
            }
            .into(goalKeeperImage)

        goalKeeperToolbar.setNavigationOnClickListener { App.router.exit() }
        goalKeeperName.text = "${state.name} ${state.surname}"

        goalKeeperAge.apply {
            findViewById<TextView>(R.id.partItemText).text = "Возраст"
            findViewById<TextView>(R.id.partItemDescription).text = "${state.age} лет"
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }
        goalKeeperQualification.apply {
            findViewById<TextView>(R.id.partItemText).text = "Квалификация"
            findViewById<TextView>(R.id.partItemDescription).text = state.qualification
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }
        goalKeeperHeight.apply {
            findViewById<TextView>(R.id.partItemText).text = "Рост"
            findViewById<TextView>(R.id.partItemDescription).text = "${state.height} см"
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }
        goalKeeperWeight.apply {
            findViewById<TextView>(R.id.partItemText).text = "Вес"
            findViewById<TextView>(R.id.partItemDescription).text = "${state.weight} кг"
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }
        goalKeeperHandside.apply {
            findViewById<TextView>(R.id.partItemText).text = "Хват"
            findViewById<TextView>(R.id.partItemDescription).text = HandSide.getHandSideById(state.handSide)
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }
        goalKeeperPrice.apply {
            findViewById<TextView>(R.id.partItemText).text = "Ценник за игру"
            findViewById<TextView>(R.id.partItemDescription).text = "От ${state.priceFrom} \u20BD до ${state.priceTo} \u20BD"
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }
        goalKeeperMetro.apply {
            findViewById<TextView>(R.id.partItemText).text = "Где удобно играть"
            findViewById<TextView>(R.id.partItemDescription).text = App.subways.find { it.id == state.metroId }!!.name
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }
        goalKeeperFind.apply {
            isVisible = false
            findViewById<TextView>(R.id.partItemText).text = "Ищу"
            findViewById<TextView>(R.id.partItemDescription).text = "" //todo
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }

        accept_button.isVisible = iceOffer != null
        reject_button.isVisible = iceOffer != null
        accept_button.setOnClickListener {
            if (iceOffer != null) {
                accept_button.isVisible = false
                reject_button.isVisible = false

                val updates = hashMapOf<String, Any>(
                    "members" to FieldValue.arrayUnion(state.userId),
                    "membersWait" to FieldValue.arrayRemove(state.userId)
                )
                firestore.collection("IceOffers").document(iceOffer!!.id)
                    .update(updates)
                    .addOnSuccessListener {
                        arguments?.remove(REQUEST_OFFER)
                        accept_button.isVisible = false
                        reject_button.isVisible = false
                    }
                    .addOnFailureListener {}
            }
        }
        reject_button.setOnClickListener {
            if (iceOffer != null) {
                accept_button.isVisible = false
                reject_button.isVisible = false

                firestore.collection("IceOffers").document(iceOffer!!.id)
                    .update("membersWait", FieldValue.arrayRemove(state.userId))
                    .addOnSuccessListener {
                        arguments?.remove(REQUEST_OFFER)
                        accept_button.isVisible = false
                        reject_button.isVisible = false
                    }
                    .addOnFailureListener {}
            }
        }

        edit_button.isVisible = currentUser != null && currentUser.uid == state.userId
        edit_button.setOnClickListener {
            EditActivity.setUser(state)
            startActivity(Intent(context!!, EditActivity::class.java))
        }

        chat_button.isVisible = currentUser != null && currentUser.uid != state.userId
        chat_button.setOnClickListener {
            val user = User(
                state.userId,
                state.name,
                state.avatarUrl
            )
            startActivity(ChatActivity.getIntent(context!!, user))
        }
    }

    override fun onBackPressed() {
        App.router.exit()
    }
}
