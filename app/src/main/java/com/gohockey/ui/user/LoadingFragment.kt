package com.gohockey.ui.user

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.IceOffer
import com.gohockey.presentation.user.LoadingPresenter
import com.gohockey.presentation.user.LoadingView
import com.gohockey.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_loading.*

class LoadingFragment : BaseFragment(), LoadingView {

    override val layoutRes = R.layout.fragment_loading

    @InjectPresenter
    lateinit var presenter: LoadingPresenter

    @ProvidePresenter
    fun providePresenter() = LoadingPresenter(
        arguments!!.getString(ARG_USER_ID)!!,
        arguments?.getParcelable(ARG_ICE_OFFER)
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }

    override fun onBackPressed() {
        App.router.exit()
    }

    override fun showMessage(message: String) {
        progress_bar.isVisible = message.isEmpty()
        error_text_view.text = message
        error_text_view.isVisible = message.isNotEmpty()
    }

    companion object {
        private const val ARG_USER_ID = "loading user id"
        private const val ARG_ICE_OFFER = "request ice offer"

        fun newInstance(userId: String, iceOffer: IceOffer?) = LoadingFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_USER_ID, userId)
                putParcelable(ARG_ICE_OFFER, iceOffer)
            }
        }
    }
}