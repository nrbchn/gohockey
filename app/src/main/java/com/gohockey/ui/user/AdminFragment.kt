package com.gohockey.ui.user

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.IceOffer
import com.gohockey.model.User
import com.gohockey.model.old.Admin
import com.gohockey.ui.chat.ChatActivity
import com.gohockey.ui.edit.profile.EditActivity
import com.gohockey.ui.global.BaseFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_admin.*

class AdminFragment : BaseFragment() {

    companion object {

        const val REQUEST_CODE = 998

        private const val STATE = "STATE"
        private const val REQUEST_OFFER = "request ice offer"

        fun newInstance(admin: Admin, iceOffer: IceOffer? = null): AdminFragment {
            val fragment = AdminFragment()
            fragment.arguments = Bundle().apply {
                putParcelable(STATE, admin)
                putParcelable(REQUEST_OFFER, iceOffer)
            }
            return fragment
        }

    }

    override val layoutRes = R.layout.fragment_admin

    private val currentUser = FirebaseAuth.getInstance().currentUser
    private val firestore = FirebaseFirestore.getInstance()

    private val state: Admin by lazy { arguments?.getParcelable(STATE) as Admin }
    private val iceOffer: IceOffer? get() = arguments?.getParcelable(REQUEST_OFFER)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(this)
            .load(state.avatarUrl)
            .apply {
                placeholder(R.drawable.ic_user_placeholder_60)
                error(R.drawable.ic_user_placeholder_60)
                circleCrop()
            }
            .into(adminImage)

        adminToolbar.setNavigationOnClickListener { App.router.exit() }
        adminName.text = "${state.name} ${state.surname}"

        adminWork.apply {
            findViewById<TextView>(R.id.partItemText).text = "Место работы"
            val work = state.work
            findViewById<TextView>(R.id.partItemDescription).text = if (work.isEmpty()) "Не указано" else work
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }
        adminMetro.apply {
            findViewById<TextView>(R.id.partItemText).text = "Где удобно играть"
            findViewById<TextView>(R.id.partItemDescription).text = App.subways.find { it.id == state.metroId }!!.name
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }
        adminPhone.apply {
            findViewById<TextView>(R.id.partItemText).text = "Телефон"
            findViewById<TextView>(R.id.partItemDescription).text = state.telephone
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }
        adminAdditaionInformation.apply {
            isVisible = state.additaionInformation.isNotEmpty()
            findViewById<TextView>(R.id.partItemText).text = "Дополнительная инфомация"
            findViewById<TextView>(R.id.partItemDescription).text = state.additaionInformation
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }

        accept_button.isVisible = iceOffer != null
        reject_button.isVisible = iceOffer != null
        accept_button.setOnClickListener {
            if (iceOffer != null) {
                accept_button.isVisible = false
                reject_button.isVisible = false

                val updates = hashMapOf<String, Any>(
                    "members" to FieldValue.arrayUnion(state.userId),
                    "membersWait" to FieldValue.arrayRemove(state.userId)
                )
                firestore.collection("IceOffers").document(iceOffer!!.id)
                    .update(updates)
                    .addOnSuccessListener {
                        arguments?.remove(REQUEST_OFFER)
                        accept_button.isVisible = false
                        reject_button.isVisible = false
                    }
                    .addOnFailureListener {}
            }
        }
        reject_button.setOnClickListener {
            if (iceOffer != null) {
                accept_button.isVisible = false
                reject_button.isVisible = false

                firestore.collection("IceOffers").document(iceOffer!!.id)
                    .update("membersWait", FieldValue.arrayRemove(state.userId))
                    .addOnSuccessListener {
                        arguments?.remove(REQUEST_OFFER)
                        accept_button.isVisible = false
                        reject_button.isVisible = false
                    }
                    .addOnFailureListener {}
            }
        }

        edit_button.isVisible = currentUser != null && currentUser.uid == state.userId
        edit_button.setOnClickListener {
            EditActivity.setUser(state)
            startActivity(Intent(context!!, EditActivity::class.java))
        }

        chat_button.isVisible = currentUser != null && currentUser.uid != state.userId
        chat_button.setOnClickListener {
            val user = User(
                state.userId,
                state.name,
                state.avatarUrl
            )
            startActivity(ChatActivity.getIntent(context!!, user))
        }
    }

    override fun onBackPressed() {
        App.router.exit()
    }
}
