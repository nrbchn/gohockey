package com.gohockey.ui.register

import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.gohockey.App
import com.gohockey.R
import com.gohockey.ui.global.BaseFragment
import com.gohockey.ui.global.ProgressDialog
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_register_email.*

class EmailFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_register_email

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        email_button.setOnClickListener {
            val email = email_edit_text.text.toString().trim()
            if (email.isEmpty()) {
                showAlert(message = "Введите e-mail")
            } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                showAlert(message = "Неправильный e-mail")
            } else {
                showProgress(true)
                FirebaseAuth.getInstance().fetchSignInMethodsForEmail(email)
                    .addOnSuccessListener {
                        if (it.signInMethods.isNullOrEmpty()) {
                            RegisterActivity.email = email
                            App.router.navigateTo(Screens.Password)
                        } else {
                            showAlert(message = "E-mail занят")
                        }
                    }
                    .addOnFailureListener {
                        Log.e("EmailFragment", it.message, it)
                        showAlert(message = it.localizedMessage)
                    }
                    .addOnCompleteListener { showProgress(false) }
            }
        }
    }

    private fun showAlert(title: String? = null, message: String) {
        AlertDialog.Builder(context!!)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("Ок", null)
            .show()
    }

    private fun showProgress(show: Boolean) {
        if (!isAdded) return

        val fragment = childFragmentManager.findFragmentByTag("progress dialog tag")
        if (fragment != null && !show) {
            (fragment as ProgressDialog).dismissAllowingStateLoss()
            childFragmentManager.executePendingTransactions()
        } else if (fragment == null && show) {
            ProgressDialog().show(childFragmentManager, "progress dialog tag")
            childFragmentManager.executePendingTransactions()
        }
    }

    override fun onBackPressed() {
        RegisterActivity.email = ""
        App.router.exit()
    }
}