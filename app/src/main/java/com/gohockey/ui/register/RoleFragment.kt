package com.gohockey.ui.register

import android.os.Bundle
import android.view.View
import androidx.core.view.setPadding
import androidx.recyclerview.widget.GridLayoutManager
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.member.MemberRole
import com.gohockey.ui.global.BaseFragment
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import kotlinx.android.synthetic.main.fragment_register_role.*
import kotlinx.android.synthetic.main.layout_base_list.*
import android.app.Activity
import android.view.inputmethod.InputMethodManager

class RoleFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_register_role

    private val adapter by lazy { RoleAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        swipe_refresh.isEnabled = false
        with(recycler_view) {
            layoutManager = GridLayoutManager(context, 2)
            adapter = this@RoleFragment.adapter
            setPadding(resources.getDimensionPixelOffset(R.dimen.card_margin))
            clipToPadding = false
            this@RoleFragment.adapter.setData(listOf(
                MemberRole.PLAYER,
                MemberRole.KEEPER,
                MemberRole.ADMIN
            ))
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val imm = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as? InputMethodManager
        val view = activity?.currentFocus ?: View(activity)
        imm?.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onBackPressed() {
        RegisterActivity.role = null
        App.router.exit()
    }

    private inner class RoleAdapter : ListDelegationAdapter<MutableList<Any>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(RoleAdapterDelegate {
                RegisterActivity.role = it
                App.router.navigateTo(Screens.Profile)
            })
        }

        fun setData(data: List<Any>) {
            items.clear()
            items.addAll(data)
            notifyDataSetChanged()
        }
    }
}