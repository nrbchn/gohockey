package com.gohockey.ui.register

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gohockey.R
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import kotlinx.android.synthetic.main.item_post_category.view.*
import com.gohockey.model.member.MemberRole

class RoleAdapterDelegate(private val clickListener: (MemberRole) -> Unit) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int): Boolean =
        items[position] is MemberRole

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_post_category, parent, false))

    override fun onBindViewHolder(
        items: MutableList<Any>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {
        (holder as ViewHolder).bind(items[position] as MemberRole)
    }

    private inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private lateinit var item: MemberRole

        init {
            view.setOnClickListener { clickListener(item) }
        }

        fun bind(item: MemberRole) {
            this.item = item

            with(itemView) {
                post_category_image_view.setImageResource(item.iconResId)
                post_category_text_view.text = item.desc
            }
        }
    }
}