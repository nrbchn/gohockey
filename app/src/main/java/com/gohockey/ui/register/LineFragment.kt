package com.gohockey.ui.register

import android.os.Bundle
import android.view.View
import com.gohockey.App
import com.gohockey.R
import com.gohockey.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_select_line.*

class LineFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_select_line

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { App.router.exit() }
        defender_button.setOnClickListener {
            RegisterActivity.line = "Защитник"
            App.router.exit()
        }
        forward_button.setOnClickListener {
            RegisterActivity.line = "Нападающий"
            App.router.exit()
        }
    }

    override fun onBackPressed() {
        App.router.exit()
    }
}