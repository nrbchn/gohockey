package com.gohockey.ui.register

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.gohockey.App
import com.gohockey.R
import com.gohockey.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_register_password.*

class PasswordFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_register_password

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        password_button.setOnClickListener {
            val password = password_edit_text.text.toString().trim()
            val confirm = password_confirm_edit_text.text.toString().trim()
            if (password.isEmpty()) {
                showAlert(message = "Введите пароль")
            } else if (password.length < 6) {
                showAlert(message = "Длина пароля должна быть не меньше 6 символов")
            } else if (password != confirm) {
                showAlert(message = "Пароли не совпадают")
            } else {
                RegisterActivity.password = password
                App.router.navigateTo(Screens.Role)
            }
        }
    }

    private fun showAlert(title: String? = null, message: String) {
        AlertDialog.Builder(context!!)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("Ок", null)
            .show()
    }

    override fun onBackPressed() {
        RegisterActivity.password = ""
        App.router.exit()
    }
}