package com.gohockey.ui.register

import android.os.Bundle
import android.view.View
import com.gohockey.App
import com.gohockey.R
import com.gohockey.ui.global.BaseFragment
import com.jaygoo.widget.OnRangeChangedListener
import com.jaygoo.widget.RangeSeekBar
import kotlinx.android.synthetic.main.fragment_select_price.*
import kotlin.math.roundToInt

class PriceFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_select_price

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { App.router.exit() }
        price_seek_bar.setProgress(RegisterActivity.priceStart, RegisterActivity.priceEnd)
        price_text_view.text = "Ценник от ${price_seek_bar.leftSeekBar.progress.roundToInt()} \u20BD до ${price_seek_bar.rightSeekBar.progress.roundToInt()} \u20BD"
        price_seek_bar.setOnRangeChangedListener(object : OnRangeChangedListener {

            override fun onRangeChanged(view: RangeSeekBar?, leftValue: Float, rightValue: Float, isFromUser: Boolean) {
                price_text_view.text = "Ценник от ${leftValue.roundToInt()} \u20BD до ${rightValue.roundToInt()} \u20BD"
            }

            override fun onStartTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
            }

            override fun onStopTrackingTouch(view: RangeSeekBar?, isLeft: Boolean) {
            }
        })
    }

    override fun onBackPressed() {
        RegisterActivity.priceStart = price_seek_bar.leftSeekBar.progress
        RegisterActivity.priceEnd = price_seek_bar.rightSeekBar.progress
        App.router.exit()
    }
}