package com.gohockey.ui.register

import android.os.Bundle
import android.view.View
import com.gohockey.App
import com.gohockey.R
import com.gohockey.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_select_grip.*

class GripFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_select_grip

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { App.router.exit() }
        right_button.setOnClickListener {
            RegisterActivity.grip = "Правый"
            App.router.exit()
        }
        left_button.setOnClickListener {
            RegisterActivity.grip = "Левый"
            App.router.exit()
        }
    }

    override fun onBackPressed() {
        App.router.exit()
    }
}