package com.gohockey.ui.register

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.arellomobile.mvp.MvpAppCompatActivity
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.member.MemberRole
import com.gohockey.model.subway.SubwayStation
import com.gohockey.ui.global.BaseFragment
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.Command

class RegisterActivity : MvpAppCompatActivity() {
    companion object {
        const val NO_LEVEL = "Квалификация *"
        const val NO_GRIP = "Хват *"
        const val NO_LINE = "Амплуа *"
        const val NO_SUBWAY = "Ближайшая станция метро *"

        var email: String = ""
        var password: String = ""
        var role: MemberRole? = null

        var level = NO_LEVEL
        var grip = NO_GRIP
        var line = NO_LINE
        var subway: SubwayStation? = null

        var priceStart = 0f
        var priceEnd = 0f
        var avatarUri: Uri? = null
    }

    private val currentFragment: BaseFragment?
        get() = supportFragmentManager.findFragmentById(R.id.content_layout) as? BaseFragment

    private val navigator: Navigator =
        object : SupportAppNavigator(this, supportFragmentManager, R.id.content_layout) {
            override fun setupFragmentTransaction(
                command: Command?,
                currentFragment: Fragment?,
                nextFragment: Fragment?,
                fragmentTransaction: FragmentTransaction
            ) {
                fragmentTransaction.setReorderingAllowed(true)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_container)

        if (savedInstanceState == null) {
            App.router.newRootScreen(Screens.Email)
        }
    }

    override fun onResume() {
        super.onResume()
        App.navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        App.navigatorHolder.removeNavigator()
    }

    override fun onBackPressed() {
        currentFragment?.onBackPressed() ?: super.onBackPressed()
    }
}

object Screens {
    object Email : SupportAppScreen() {
        override fun getFragment() = EmailFragment()
    }
    object Password : SupportAppScreen() {
        override fun getFragment() = PasswordFragment()
    }
    object Role : SupportAppScreen() {
        override fun getFragment() = RoleFragment()
    }
    object Profile : SupportAppScreen() {
        override fun getFragment() = ProfileFragment()
    }
    object Level : SupportAppScreen() {
        override fun getFragment() = LevelFragment()
    }
    object Grip : SupportAppScreen() {
        override fun getFragment() = GripFragment()
    }
    object Line : SupportAppScreen() {
        override fun getFragment() = LineFragment()
    }
    object Subway : SupportAppScreen() {
        override fun getFragment() = SubwayFragment()
    }
    object Price : SupportAppScreen() {
        override fun getFragment() = PriceFragment()
    }
}