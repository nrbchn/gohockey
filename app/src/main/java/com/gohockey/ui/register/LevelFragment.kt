package com.gohockey.ui.register

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.view.isVisible
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.member.MemberRole
import com.gohockey.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_select_level.*

class LevelFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_select_level

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { App.router.exit() }
        level_1_button.setOnClickListener {
            RegisterActivity.level = (it as TextView).text.toString()
            App.router.exit()
        }
        level_2_button.setOnClickListener {
            RegisterActivity.level = (it as TextView).text.toString()
            App.router.exit()
        }
        level_3_button.setOnClickListener {
            RegisterActivity.level = (it as TextView).text.toString()
            App.router.exit()
        }
        level_4_button.setOnClickListener {
            RegisterActivity.level = (it as TextView).text.toString()
            App.router.exit()
        }
        level_5_button.setOnClickListener {
            RegisterActivity.level = (it as TextView).text.toString()
            App.router.exit()
        }
        level_6_button.setOnClickListener {
            RegisterActivity.level = (it as TextView).text.toString()
            App.router.exit()
        }
        level_7_button.setOnClickListener {
            RegisterActivity.level = (it as TextView).text.toString()
            App.router.exit()
        }
        level_2_button.isVisible = RegisterActivity.role == MemberRole.PLAYER
        level_3_button.isVisible = RegisterActivity.role == MemberRole.PLAYER
        level_4_button.isVisible = RegisterActivity.role == MemberRole.PLAYER
        level_6_button.isVisible = RegisterActivity.role == MemberRole.PLAYER
        level_7_button.isVisible = RegisterActivity.role == MemberRole.KEEPER
        level_8_button.isVisible = RegisterActivity.role == MemberRole.KEEPER
    }

    override fun onBackPressed() {
        App.router.exit()
    }
}