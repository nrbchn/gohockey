package com.gohockey.ui.register

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.FirestoreCollections
import com.gohockey.model.member.MemberRole
import com.gohockey.ui.global.BaseFragment
import com.gohockey.ui.global.ProgressDialog
import com.gohockey.ui.main.MainActivity
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_register_profile.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import kotlin.math.roundToInt

class ProfileFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_register_profile

    private val rxPermissions by lazy { RxPermissions(this) }
    private var disposable: Disposable? = null
    private var outputFileUri: Uri? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        level_button.setOnClickListener { App.router.navigateTo(Screens.Level) }
        grip_button.setOnClickListener { App.router.navigateTo(Screens.Grip) }
        line_button.setOnClickListener { App.router.navigateTo(Screens.Line) }
        subway_button.setOnClickListener { App.router.navigateTo(Screens.Subway) }
        price_button.setOnClickListener { App.router.navigateTo(Screens.Price) }
        age_edit_text.isVisible = RegisterActivity.role != MemberRole.ADMIN
        keeper_switch.isVisible = RegisterActivity.role == MemberRole.KEEPER
        phone_edit_text.isVisible = RegisterActivity.role == MemberRole.ADMIN
        level_button.isVisible = RegisterActivity.role != MemberRole.ADMIN
        height_edit_text.isVisible = RegisterActivity.role != MemberRole.ADMIN
        weight_edit_text.isVisible = RegisterActivity.role != MemberRole.ADMIN
        grip_button.isVisible = RegisterActivity.role != MemberRole.ADMIN
        line_button.isVisible = RegisterActivity.role == MemberRole.PLAYER
        price_button.isVisible = RegisterActivity.role == MemberRole.KEEPER
        info_edit_text.isVisible = RegisterActivity.role == MemberRole.ADMIN
        add_image_button.setOnClickListener {
            AlertDialog.Builder(context!!)
                .setTitle("Загрузить фото")
                .setItems(arrayOf("Сделать фотографию", "Выбрать из галереи")) { _, position ->
                    when (position) {
                        0 -> {
                            disposable?.dispose()
                            disposable = rxPermissions.requestEach(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                .subscribe {
                                    if (it.granted) {
                                        openCamera()
                                    }
                                }
                        }
                        1 -> {
                            disposable?.dispose()
                            disposable = rxPermissions.requestEach(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                .subscribe {
                                    if (it.granted) {
                                        pickFile()
                                    }
                                }
                        }
                    }
                }
                .show()
        }
        done_button.setOnClickListener {
            when (RegisterActivity.role) {
                MemberRole.PLAYER -> {
                    if (name_edit_text.text.toString().trim().isEmpty() ||
                        surname_edit_text.text.toString().trim().isEmpty() ||
                        age_edit_text.text.toString().trim().isEmpty() ||
                        height_edit_text.text.toString().trim().isEmpty() ||
                        weight_edit_text.text.toString().trim().isEmpty() ||
                        RegisterActivity.level == RegisterActivity.NO_LEVEL ||
                        RegisterActivity.grip == RegisterActivity.NO_GRIP ||
                        RegisterActivity.line == RegisterActivity.NO_LINE ||
                        RegisterActivity.subway == null
                    ) {

                        showAlert(message = "Заполните все поля")
                    } else {
                        if (RegisterActivity.avatarUri == null) {
                            showAlert(message = "Загрузите фотографию")
                            return@setOnClickListener
                        }

                        showProgress(true)
                        FirebaseAuth.getInstance().createUserWithEmailAndPassword(RegisterActivity.email, RegisterActivity.password)
                            .addOnSuccessListener {
                                val userId = it.user.uid
                                val db = FirebaseFirestore.getInstance()

                                image_view.isDrawingCacheEnabled = true
                                image_view.buildDrawingCache()
                                val bitmap = (image_view.drawable as BitmapDrawable).bitmap
                                val baos = ByteArrayOutputStream()
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                                val data = baos.toByteArray()

                                val ref = FirebaseStorage.getInstance().reference.child("profiles/${userId}")
                                ref.putBytes(data)
                                    .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                                        if (!task.isSuccessful) {
                                            task.exception?.let {
                                                throw it
                                            }
                                        }
                                        return@Continuation ref.downloadUrl
                                    }).addOnCompleteListener { task ->
                                        if (task.isSuccessful) {
                                            val downloadUri = task.result
                                            val avatarUrl = downloadUri.toString()

                                            db.collection(FirestoreCollections.USERS)
                                                .document(userId)
                                                .set(hashMapOf(
                                                    "email" to RegisterActivity.email,
                                                    "id" to userId,
                                                    "idRole" to RegisterActivity.role!!.desc,
                                                    "telephone" to null
                                                ))

                                            db.collection(FirestoreCollections.PLAYER)
                                                .document(userId)
                                                .set(hashMapOf(
                                                    "age" to age_edit_text.text.toString().trim(),
                                                    "avatarUrl" to avatarUrl,
                                                    "createdAt" to FieldValue.serverTimestamp(),
                                                    "find" to "",
                                                    "handSide" to when (RegisterActivity.grip) {
                                                        "Правый" -> 1
                                                        "Левый" -> 0
                                                        else -> 1
                                                    },
                                                    "height" to height_edit_text.text.toString().trim(),
                                                    "weight" to weight_edit_text.text.toString().trim(),
                                                    "idRole" to RegisterActivity.role!!.desc,
                                                    "metroId" to RegisterActivity.subway!!.id,
                                                    "qualification" to RegisterActivity.level,
                                                    "role" to RegisterActivity.line,
                                                    "name" to name_edit_text.text.toString().trim(),
                                                    "surname" to surname_edit_text.text.toString().trim(),
                                                    "userId" to userId
                                                ))
                                                .addOnSuccessListener {
                                                    clearFields()
                                                    startActivity(MainActivity.getIntent(context!!))
                                                    activity?.finish()
                                                }
                                                .addOnFailureListener {
                                                    showAlert(message = it.localizedMessage)
                                                }
                                                .addOnCompleteListener { showProgress(false) }
                                        } else {
                                            showProgress(false)
                                        }
                                    }
                            }
                            .addOnFailureListener {
                                showProgress(false)
                                showAlert(message = it.localizedMessage)
                            }
                    }
                }
                MemberRole.KEEPER -> {
                    if (name_edit_text.text.toString().trim().isEmpty() ||
                        surname_edit_text.text.toString().trim().isEmpty() ||
                        age_edit_text.text.toString().trim().isEmpty() ||
                        height_edit_text.text.toString().trim().isEmpty() ||
                        weight_edit_text.text.toString().trim().isEmpty() ||
                        RegisterActivity.level == RegisterActivity.NO_LEVEL ||
                        RegisterActivity.grip == RegisterActivity.NO_GRIP ||
                        RegisterActivity.subway == null
                    ) {

                        showAlert(message = "Заполните все поля")
                    } else {
                        if (RegisterActivity.avatarUri == null) {
                            showAlert(message = "Загрузите фотографию")
                            return@setOnClickListener
                        }

                        showProgress(true)
                        FirebaseAuth.getInstance().createUserWithEmailAndPassword(RegisterActivity.email, RegisterActivity.password)
                            .addOnSuccessListener {
                                val userId = it.user.uid
                                val db = FirebaseFirestore.getInstance()

                                image_view.isDrawingCacheEnabled = true
                                image_view.buildDrawingCache()
                                val bitmap = (image_view.drawable as BitmapDrawable).bitmap
                                val baos = ByteArrayOutputStream()
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                                val data = baos.toByteArray()

                                val ref = FirebaseStorage.getInstance().reference.child("profiles/$userId")
                                ref.putBytes(data)
                                    .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                                        if (!task.isSuccessful) {
                                            task.exception?.let {
                                                throw it
                                            }
                                        }
                                        return@Continuation ref.downloadUrl
                                    }).addOnCompleteListener { task ->
                                        if (task.isSuccessful) {
                                            val downloadUri = task.result
                                            val avatarUrl = downloadUri.toString()
                                            db.collection(FirestoreCollections.USERS)
                                                .document(userId)
                                                .set(hashMapOf(
                                                    "email" to RegisterActivity.email,
                                                    "id" to userId,
                                                    "idRole" to RegisterActivity.role!!.desc,
                                                    "telephone" to null
                                                ))

                                            db.collection(FirestoreCollections.KEEPER)
                                                .document(userId)
                                                .set(hashMapOf(
                                                    "age" to age_edit_text.text.toString().trim(),
                                                    "avatarUrl" to avatarUrl,
                                                    "createdAt" to FieldValue.serverTimestamp(),
                                                    "handSide" to when (RegisterActivity.grip) {
                                                        "Правый" -> 1
                                                        "Левый" -> 0
                                                        else -> 1
                                                    },
                                                    "height" to height_edit_text.text.toString().trim(),
                                                    "idRole" to RegisterActivity.role!!.desc,
                                                    "isVisible" to keeper_switch.isChecked,
                                                    "metroId" to RegisterActivity.subway!!.id,
                                                    "name" to name_edit_text.text.toString().trim(),
                                                    "priceFrom" to RegisterActivity.priceStart.roundToInt().toString(),
                                                    "priceTo" to RegisterActivity.priceEnd.roundToInt().toString(),
                                                    "qualification" to RegisterActivity.level,
                                                    "surname" to surname_edit_text.text.toString().trim(),
                                                    "userId" to userId,
                                                    "weight" to weight_edit_text.text.toString().trim()
                                                ))
                                                .addOnSuccessListener {
                                                    clearFields()
                                                    startActivity(MainActivity.getIntent(context!!))
                                                    activity?.finish()
                                                }
                                                .addOnFailureListener {
                                                    showAlert(message = it.localizedMessage)
                                                }
                                                .addOnCompleteListener { showProgress(false) }
                                        } else {
                                            showProgress(false)
                                        }
                                    }
                            }
                            .addOnFailureListener {
                                showProgress(false)
                                showAlert(message = it.localizedMessage)
                            }
                    }
                }
                MemberRole.ADMIN -> {
                    if (name_edit_text.text.toString().trim().isEmpty() ||
                        surname_edit_text.text.toString().trim().isEmpty() ||
                        phone_edit_text.text.toString().trim().isEmpty() ||
                        RegisterActivity.subway == null
                    ) {

                        showAlert(message = "Заполните все поля")
                    } else {
                        if (RegisterActivity.avatarUri == null) {
                            showAlert(message = "Загрузите фотографию")
                            return@setOnClickListener
                        }

                        showProgress(true)
                        FirebaseAuth.getInstance().createUserWithEmailAndPassword(RegisterActivity.email, RegisterActivity.password)
                            .addOnSuccessListener {
                                val userId = it.user.uid
                                val db = FirebaseFirestore.getInstance()

                                image_view.isDrawingCacheEnabled = true
                                image_view.buildDrawingCache()
                                val bitmap = (image_view.drawable as BitmapDrawable).bitmap
                                val baos = ByteArrayOutputStream()
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                                val data = baos.toByteArray()

                                val ref = FirebaseStorage.getInstance().reference.child("profiles/${userId}")
                                ref.putBytes(data)
                                    .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                                        if (!task.isSuccessful) {
                                            task.exception?.let {
                                                throw it
                                            }
                                        }
                                        return@Continuation ref.downloadUrl
                                    })
                                    .addOnCompleteListener { task ->
                                        if (task.isSuccessful) {
                                            val downloadUri = task.result
                                            val avatarUrl = downloadUri.toString()

                                            db.collection(FirestoreCollections.USERS)
                                                .document(userId)
                                                .set(hashMapOf(
                                                    "email" to RegisterActivity.email,
                                                    "id" to userId,
                                                    "idRole" to RegisterActivity.role!!.desc,
                                                    "telephone" to phone_edit_text.text.toString().trim()
                                                ))

                                            db.collection(FirestoreCollections.ADMIN)
                                                .document(userId)
                                                .set(hashMapOf(
                                                    "additaionInformation" to info_edit_text.text.toString().trim(),
                                                    "avatarUrl" to avatarUrl,
                                                    "createdAt" to FieldValue.serverTimestamp(),
                                                    "idRole" to RegisterActivity.role!!.desc,
                                                    "metroId" to RegisterActivity.subway!!.id,
                                                    "name" to name_edit_text.text.toString().trim(),
                                                    "surname" to surname_edit_text.text.toString().trim(),
                                                    "teamId" to null,
                                                    "telephone" to phone_edit_text.text.toString().trim(),
                                                    "userId" to userId,
                                                    "work" to ""
                                                ))
                                                .addOnSuccessListener {
                                                    clearFields()
                                                    startActivity(MainActivity.getIntent(context!!))
                                                    activity?.finish()
                                                }
                                                .addOnFailureListener {
                                                    showAlert(message = it.localizedMessage)
                                                }
                                                .addOnCompleteListener { showProgress(false) }
                                        } else {
                                            showProgress(false)
                                        }
                                    }
                            }
                            .addOnFailureListener {
                                showProgress(false)
                                showAlert(message = it.localizedMessage)
                            }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        level_button.text = RegisterActivity.level
        grip_button.text = RegisterActivity.grip
        line_button.text = RegisterActivity.line
        subway_button.text = RegisterActivity.subway?.name ?: RegisterActivity.NO_SUBWAY
        Glide.with(this)
            .load(RegisterActivity.avatarUri)
            .apply {
                placeholder(R.drawable.ic_user_placeholder_60)
                error(R.drawable.ic_user_placeholder_60)
                circleCrop()
            }
            .into(image_view)
    }

    private fun showAlert(title: String? = null, message: String) {
        AlertDialog.Builder(context!!)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("Ок", null)
            .show()
    }

    private fun showProgress(show: Boolean) {
        if (!isAdded) return

        val fragment = childFragmentManager.findFragmentByTag("progress dialog tag")
        if (fragment != null && !show) {
            (fragment as ProgressDialog).dismissAllowingStateLoss()
            childFragmentManager.executePendingTransactions()
        } else if (fragment == null && show) {
            ProgressDialog().show(childFragmentManager, "progress dialog tag")
            childFragmentManager.executePendingTransactions()
        }
    }

    private fun clearFields() {
        RegisterActivity.email = ""
        RegisterActivity.password = ""
        RegisterActivity.role = null
        RegisterActivity.level = RegisterActivity.NO_LEVEL
        RegisterActivity.grip = RegisterActivity.NO_GRIP
        RegisterActivity.line = RegisterActivity.NO_LINE
        RegisterActivity.subway = null
        RegisterActivity.priceStart = 0f
        RegisterActivity.priceEnd = 0f
        RegisterActivity.avatarUri = null
    }

    override fun onBackPressed() {
        RegisterActivity.level = RegisterActivity.NO_LEVEL
        RegisterActivity.grip = RegisterActivity.NO_GRIP
        RegisterActivity.line = RegisterActivity.NO_LINE
        RegisterActivity.subway = null
        RegisterActivity.priceStart = 0f
        RegisterActivity.priceEnd = 0f
        RegisterActivity.avatarUri = null
        App.router.exit()
    }

    private fun pickFile() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        try {
            startActivityForResult(
                Intent.createChooser(intent, "Выбрать "),
                FILE_SELECT_CODE
            )
        } catch (e: Exception) {
            Log.e("ProfileFragment", e.message, e)
        }
    }

    private fun openCamera() {
        val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePhotoIntent.resolveActivity(context!!.packageManager) != null) {
            val photoFile = try {
                val storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                storageDirectory.mkdir()
                File.createTempFile("IMAGE_${System.currentTimeMillis()}", ".jpg", storageDirectory)
            } catch (e: IOException) {
                Log.e("ProfileFragment", e.message, e)
                null
            }
            photoFile?.let {
                outputFileUri = FileProvider.getUriForFile(context!!, "${context!!.applicationContext!!.packageName}.provider", it)
                takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)
                startActivityForResult(takePhotoIntent, CAMERA_REQUEST_CODE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        when (requestCode) {
            FILE_SELECT_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    intent?.data?.let {
                        RegisterActivity.avatarUri = it
                    }
                }
            }
            CAMERA_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    outputFileUri?.let {
                        RegisterActivity.avatarUri = it
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        disposable?.dispose()
        super.onDestroyView()
    }

    companion object {
        private const val CAMERA_REQUEST_CODE = 101
        private const val FILE_SELECT_CODE = 185
    }
}