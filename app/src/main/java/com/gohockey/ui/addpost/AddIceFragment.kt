package com.gohockey.ui.addpost

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.FirestoreCollections
import com.gohockey.model.IceOffer
import com.gohockey.ui.global.BaseFragment
import com.gohockey.ui.global.ProgressDialog
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_add_ice.*
import java.text.SimpleDateFormat
import java.util.*
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog

class AddIceFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_add_ice

    private var dialog: SingleDateAndTimePickerDialog? = null

    private val currentUser = FirebaseAuth.getInstance().currentUser
    private val firestore = FirebaseFirestore.getInstance()
    private val formatter = SimpleDateFormat("dd.MM.yyyy в HH:mm", Locale.getDefault())

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setNavigationOnClickListener { onBackPressed() }

        start_button.setOnClickListener {
            dialog?.close()

            val builder = SingleDateAndTimePickerDialog.Builder(context)
                .curved()
                .mustBeOnFuture()
                .mainColor(ContextCompat.getColor(context!!, R.color.colorAccent))
                .titleTextColor(Color.WHITE)
                .title("Время начала")
                .listener {
                    val datetime = formatter.format(it)
                    AddPostActivity.start = datetime
                    start_button.text = datetime
                }

            try {
                builder.maxDateRange(formatter.parse(AddPostActivity.end))
            } catch (e: Exception) {}

            dialog = builder.build()
            dialog?.display()
        }

        end_button.setOnClickListener {
            dialog?.close()

            val builder = SingleDateAndTimePickerDialog.Builder(context)
                .curved()
                .mustBeOnFuture()
                .mainColor(ContextCompat.getColor(context!!, R.color.colorAccent))
                .titleTextColor(Color.WHITE)
                .title("Время завершения")
                .listener {
                    val datetime = formatter.format(it)
                    AddPostActivity.end = datetime
                    end_button.text = datetime
                }

            try {
                builder.minDateRange(formatter.parse(AddPostActivity.start))
            } catch (e: Exception) {}

            dialog = builder.build()
            dialog?.display()
        }

        theme_button.setOnClickListener { App.router.navigateTo(Screens.Theme) }

        level_button.setOnClickListener { App.router.navigateTo(Screens.Level) }

        subway_button.setOnClickListener { App.router.navigateTo(Screens.Subway) }

        done_button.setOnClickListener {
            if (title_edit_text.text.toString().trim().isEmpty() ||
                price_edit_text.text.toString().trim().isEmpty() ||
                AddPostActivity.start == AddPostActivity.NO_START ||
                AddPostActivity.end == AddPostActivity.NO_END ||
                AddPostActivity.theme == AddPostActivity.NO_THEME ||
                AddPostActivity.level == AddPostActivity.NO_LEVEL ||
                AddPostActivity.subway == null
            ) {

                showAlert(message = "Заполните все поля")
            } else {
                showProgress(true)

                if (currentUser == null) {
                    //todo
                } else {
                    val ref = firestore.collection(FirestoreCollections.ICE_OFFERS).document()
                    ref.set(
                        IceOffer(
                            avatarUrl = "",
                            userId = currentUser.uid,
                            createdAt = Timestamp.now(),
                            id = ref.id,
                            members = listOf(currentUser.uid),
                            membersWait = emptyList(),
                            startDate = Timestamp(formatter.parse(AddPostActivity.start)),
                            endDate = Timestamp(formatter.parse(AddPostActivity.end)),
                            nameIce = title_edit_text.text.toString().trim(),
                            price = price_edit_text.text.toString().trim(),
                            theme = AddPostActivity.theme,
                            qualification = AddPostActivity.level,
                            metroId = AddPostActivity.subway!!.id,
                            goalKeeper = keeper_switch.isChecked,
                            coach = coach_switch.isChecked
                        )
                    )
                        .addOnSuccessListener {
                            AlertDialog.Builder(context!!)
                                .setMessage("Запись опубликована")
                                .setPositiveButton("Ок") { _, _ ->
                                    clearFields()
                                    activity?.finish()
                                }
                                .show()
                        }
                        .addOnFailureListener { showAlert(message = it.localizedMessage) }
                        .addOnCompleteListener { showProgress(false) }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        start_button.text = AddPostActivity.start
        end_button.text = AddPostActivity.end
        theme_button.text = AddPostActivity.theme
        level_button.text = AddPostActivity.level
        subway_button.text = AddPostActivity.subway?.name ?: AddPostActivity.NO_SUBWAY
    }

    private fun showAlert(title: String? = null, message: String) {
        AlertDialog.Builder(context!!)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("Ок", null)
            .show()
    }

    private fun showProgress(show: Boolean) {
        if (!isAdded) return

        val fragment = childFragmentManager.findFragmentByTag("progress dialog tag")
        if (fragment != null && !show) {
            (fragment as ProgressDialog).dismissAllowingStateLoss()
            childFragmentManager.executePendingTransactions()
        } else if (fragment == null && show) {
            ProgressDialog().show(childFragmentManager, "progress dialog tag")
            childFragmentManager.executePendingTransactions()
        }
    }

    private fun clearFields() {
        AddPostActivity.start = AddPostActivity.NO_START
        AddPostActivity.end = AddPostActivity.NO_END
        AddPostActivity.level = AddPostActivity.NO_LEVEL
        AddPostActivity.theme = AddPostActivity.NO_THEME
        AddPostActivity.subway = null
    }

    override fun onBackPressed() {
        if (dialog?.isDisplaying == true) {
            dialog?.close()
            return
        }
        clearFields()
        App.router.exit()
    }
}