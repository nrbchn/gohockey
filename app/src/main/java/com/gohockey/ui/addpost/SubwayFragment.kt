package com.gohockey.ui.addpost

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.gohockey.App
import com.gohockey.R
import com.gohockey.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_select_subway.*

class SubwayFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_select_subway

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { App.router.exit() }

        App.subways.forEach { station ->
            val text_view = LayoutInflater.from(context).inflate(R.layout.layout_subway_item, null) as TextView
            text_view.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            text_view.text = station.name
            text_view.setOnClickListener {
                AddPostActivity.subway = station
                App.router.exit()
            }
            subway_layout.addView(text_view)
        }
    }

    override fun onBackPressed() {
        App.router.exit()
    }
}