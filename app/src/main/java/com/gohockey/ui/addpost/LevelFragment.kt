package com.gohockey.ui.addpost

import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.gohockey.App
import com.gohockey.R
import com.gohockey.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_select_level.*

class LevelFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_select_level

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { App.router.exit() }
        toolbar_title.text = "Уровень игроков"
        level_1_button.setOnClickListener {
            AddPostActivity.level = (it as TextView).text.toString()
            App.router.exit()
        }
        level_2_button.setOnClickListener {
            AddPostActivity.level = (it as TextView).text.toString()
            App.router.exit()
        }
        level_3_button.setOnClickListener {
            AddPostActivity.level = (it as TextView).text.toString()
            App.router.exit()
        }
        level_4_button.setOnClickListener {
            AddPostActivity.level = (it as TextView).text.toString()
            App.router.exit()
        }
        level_5_button.setOnClickListener {
            AddPostActivity.level = (it as TextView).text.toString()
            App.router.exit()
        }
        level_6_button.setOnClickListener {
            AddPostActivity.level = (it as TextView).text.toString()
            App.router.exit()
        }
    }

    override fun onBackPressed() {
        App.router.exit()
    }
}