package com.gohockey.ui.addpost

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.arellomobile.mvp.MvpAppCompatActivity
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.subway.SubwayStation
import com.gohockey.ui.global.BaseFragment
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.Command

class AddPostActivity : MvpAppCompatActivity() {
    companion object {
        const val NO_START = "Время начала *"
        const val NO_END = "Время завершения *"
        const val NO_THEME = "Тематика *"
        const val NO_LEVEL = "Уровень игроков *"
        const val NO_SUBWAY = "Ближайшая станция метро *"

        var start = NO_START
        var end = NO_END
        var theme = NO_THEME
        var level = NO_LEVEL
        var subway: SubwayStation? = null
    }

    private val currentFragment: BaseFragment?
        get() = supportFragmentManager.findFragmentById(R.id.content_layout) as? BaseFragment

    private val navigator: Navigator =
        object : SupportAppNavigator(this, supportFragmentManager, R.id.content_layout) {
            override fun setupFragmentTransaction(
                command: Command?,
                currentFragment: Fragment?,
                nextFragment: Fragment?,
                fragmentTransaction: FragmentTransaction
            ) {
                fragmentTransaction.setReorderingAllowed(true)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_container)

        if (savedInstanceState == null) {
            App.router.newRootScreen(Screens.AddIce)
        }
    }

    override fun onResume() {
        super.onResume()
        App.navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        App.navigatorHolder.removeNavigator()
    }

    override fun onBackPressed() {
        currentFragment?.onBackPressed() ?: super.onBackPressed()
    }
}

object Screens {
    object AddIce : SupportAppScreen() {
        override fun getFragment() = AddIceFragment()
    }
    object Theme : SupportAppScreen() {
        override fun getFragment() = ThemeFragment()
    }
    object Level : SupportAppScreen() {
        override fun getFragment() = LevelFragment()
    }
    object Subway : SupportAppScreen() {
        override fun getFragment() = SubwayFragment()
    }
}