package com.gohockey.ui.addpost

import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.gohockey.App
import com.gohockey.R
import com.gohockey.ui.global.BaseFragment
import kotlinx.android.synthetic.main.fragment_select_theme.*

class ThemeFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_select_theme

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { App.router.exit() }
        theme_1_button.setOnClickListener {
            AddPostActivity.theme = (it as TextView).text.toString()
            App.router.exit()
        }
        theme_2_button.setOnClickListener {
            AddPostActivity.theme = (it as TextView).text.toString()
            App.router.exit()
        }
        theme_3_button.setOnClickListener {
            AddPostActivity.theme = (it as TextView).text.toString()
            App.router.exit()
        }
    }

    override fun onBackPressed() {
        App.router.exit()
    }
}