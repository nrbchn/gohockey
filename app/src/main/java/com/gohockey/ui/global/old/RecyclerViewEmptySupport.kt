package com.gohockey.ui.global.old

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.core.content.withStyledAttributes
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.gohockey.R

class RecyclerViewEmptySupport @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttrs: Int = 0
) : RecyclerView(context, attributeSet, defStyleAttrs) {

    private var showEmptyViewDuringLoading = false

    init {
        context.withStyledAttributes(attributeSet, R.styleable.RecyclerViewEmptySupport, defStyleAttrs, 0) {
            showEmptyViewDuringLoading = getBoolean(R.styleable.RecyclerViewEmptySupport_showEmptyViewDuringLoading, false)
        }
    }

    private var emptyView: View? = null

    private val emptyObserver = object : AdapterDataObserver() {

        override fun onChanged() {
            val adapter = adapter
            if (adapter != null && emptyView != null) {
                showContent(adapter.itemCount != 0)
            }
        }
    }

    override fun setAdapter(adapter: Adapter<*>?) {
        super.setAdapter(adapter)

        adapter?.registerAdapterDataObserver(emptyObserver)

        if (showEmptyViewDuringLoading) {
            emptyObserver.onChanged()
        }
    }

    fun setEmptyView(emptyView: View?) {
        this.emptyView = emptyView
    }

    private fun showContent(isVisibleContent: Boolean) {
        emptyView?.isVisible = !isVisibleContent
        this.isVisible = isVisibleContent
    }

}