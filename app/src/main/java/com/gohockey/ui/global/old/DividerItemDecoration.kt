package com.gohockey.ui.global.old

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.View
import androidx.annotation.Dimension
import androidx.annotation.DrawableRes
import androidx.core.content.withStyledAttributes
import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView
import com.gohockey.model.old.ShouldNotHappenException
import kotlin.math.roundToInt

class DividerItemDecoration(
        context: Context,
        @DrawableRes drawableId: Int? = null,
        private val predicate: ((position: Int) -> Boolean) = { true },
        @Dimension(unit = Dimension.PX) private val startMargin: Int = 0,
        @Dimension(unit = Dimension.PX) private val endMargin: Int = 0,
        private val offset: Boolean = true
) : RecyclerView.ItemDecoration() {

    private val bounds = Rect()
    private lateinit var divider: Drawable

    init {
        setDivider(context, drawableId)
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val position = parent.getChildAdapterPosition(view)
        if (offset && predicate(position) && position != state.itemCount - 1) {
            outRect.set(0, 0, 0, divider.intrinsicHeight)
        }
    }

    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        if (offset) {
            drawDivider(canvas, parent, state)
        }
    }

    override fun onDrawOver(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        if (!offset) {
            drawDivider(canvas, parent, state)
        }
    }

    private fun setDivider(context: Context, drawableId: Int?) {
        if (drawableId == null) {
            context.withStyledAttributes(null, intArrayOf(android.R.attr.listDivider)) {
                divider = getDrawable(0) ?: throw ShouldNotHappenException("Can't get default drawable for recycler divider")
            }
        } else {
            divider = context.getDrawable(drawableId) ?: throw ShouldNotHappenException("Can't get drawable $drawableId for recycler divider")
        }
    }

    private fun drawDivider(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        canvas.save()
        parent.children.forEach { child ->
            val position = parent.getChildAdapterPosition(child)
            if (predicate(position) && position != state.itemCount - 1) {
                parent.getDecoratedBoundsWithMargins(child, bounds)
                val bottom = bounds.bottom + child.translationY.roundToInt()
                val top = bottom - divider.intrinsicHeight
                divider.setBounds(bounds.left + startMargin, top, bounds.right - endMargin, bottom)
                divider.draw(canvas)
            }
        }
        canvas.restore()
    }

}
