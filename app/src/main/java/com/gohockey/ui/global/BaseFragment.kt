package com.gohockey.ui.global

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.gohockey.App

private const val STATE_LAUNCH_FLAG = "state_launch_flag"

abstract class BaseFragment : MvpAppCompatFragment() {

    abstract val layoutRes: Int

    private val viewHandler = Handler()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(layoutRes, container, false)

    protected fun postViewAction(action: () -> Unit) {
        viewHandler.post(action)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewHandler.removeCallbacksAndMessages(null)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(
            STATE_LAUNCH_FLAG,
            App.appCode
        )
    }

    protected fun isFirstLaunch(savedInstanceState: Bundle?): Boolean {
        val savedAppCode = savedInstanceState?.getString(STATE_LAUNCH_FLAG)
        return savedAppCode != App.appCode
    }

    open fun onBackPressed() {}
}
