package com.gohockey.ui.members

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gohockey.R
import com.gohockey.model.IceOffer
import com.gohockey.presentation.members.MembersPresenter
import com.gohockey.presentation.members.MembersView
import com.gohockey.ui.chat.UserAdapterDelegate
import com.gohockey.ui.user.UserActivity
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import kotlinx.android.synthetic.main.activity_members.*

class MembersActivity : MvpAppCompatActivity(), MembersView {

    private val iceOffer: IceOffer get() = intent.extras!!.getParcelable(ARG_ICE_OFFER)!!

    private val adapter by lazy { UsersAdapter() }

    @InjectPresenter
    lateinit var presenter: MembersPresenter

    @ProvidePresenter
    fun providePresenter() = MembersPresenter(iceOffer)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_members)

        toolbar.setNavigationOnClickListener { onBackPressed() }

        swipe_refresh.setOnRefreshListener { presenter.refresh() }

        with(recycler_view) {
            layoutManager = LinearLayoutManager(context)
            adapter = this@MembersActivity.adapter
        }
    }

    override fun showTitle(title: String) {
        toolbar_title.text = title
    }

    override fun showData(data: List<Any>) {
        adapter.setData(data)
    }

    override fun showError(message: String?) {
        error_text_view.text = message
        error_text_view.isVisible = !message.isNullOrEmpty()
    }

    override fun showProgress(show: Boolean) {
        swipe_refresh.isRefreshing = show
    }

    private inner class UsersAdapter : ListDelegationAdapter<MutableList<Any>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(UserAdapterDelegate { startActivity(UserActivity.getIntent(this@MembersActivity, it.id)) })
        }

        fun setData(data: List<Any>) {
            items.clear()
            items.addAll(data)
            notifyDataSetChanged()
        }
    }

    companion object {
        private const val ARG_ICE_OFFER = "ice offer"

        fun getIntent(context: Context, post: IceOffer) = Intent(context, MembersActivity::class.java).apply {
            putExtra(ARG_ICE_OFFER, post)
        }
    }
}