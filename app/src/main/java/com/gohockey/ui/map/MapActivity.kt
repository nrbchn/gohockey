package com.gohockey.ui.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.OnMapReadyCallback
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.gohockey.R
import com.gohockey.ui.post.PostActivity
import com.gohockey.ui.user.UserActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.maps.model.*
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_map.*

val DEFAULT_LATLNG = LatLng(55.752220, 37.615560)

class MapActivity : FragmentActivity(), OnMapReadyCallback {

    private val rxPermissions by lazy { RxPermissions(this) }
    private var disposable: Disposable? = null
    private val mapFragment get() = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
    private val markers get() = intent.extras!!.getParcelableArrayList<Marker>(ARG_MARKERS)!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        when (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)) {
            ConnectionResult.SUCCESS -> {}
            ConnectionResult.SERVICE_MISSING -> showMessage("SERVICE_MISSING")
            ConnectionResult.SERVICE_UPDATING -> showMessage("SERVICE_UPDATING")
            ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED -> showMessage("SERVICE_VERSION_UPDATE_REQUIRED")
            ConnectionResult.SERVICE_DISABLED -> showMessage("SERVICE_DISABLED")
            ConnectionResult.SERVICE_INVALID -> showMessage("SERVICE_INVALID")
        }

        showProgress(true)
        mapFragment?.getMapAsync(this)
    }

    private fun showMessage(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        markers.forEach {
            val index: Float = markers.indexOf(it).toFloat() / markers.size
            val latLng = LatLng(it.lat, it.lon + index/8000)
            val title = if (it.iceOffer != null) {
                it.iceOffer.nameIce
            } else {
                it.user.name
            }
            val tag = it

            if (!it.user.photo.isNullOrEmpty()) {
                Glide.with(this)
                    .asBitmap()
                    .circleCrop()
                    .load(it.user.photo)
                    .into(object : SimpleTarget<Bitmap>(resources.getDimensionPixelOffset(R.dimen.marker_size), resources.getDimensionPixelOffset(R.dimen.marker_size)) {

                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            val googleMarker = googleMap.addMarker(
                                MarkerOptions()
                                    .icon(BitmapDescriptorFactory.fromBitmap(resource))
                                    .position(latLng)
                                    .zIndex(index)
                                    .title(title)
                            )
                            googleMarker.tag = tag
                        }

                        override fun onLoadFailed(errorDrawable: Drawable?) {
                            val googleMarker = googleMap.addMarker(
                                MarkerOptions()
                                    .icon(bitmapDescriptorFromVector(this@MapActivity, R.drawable.ic_marker_placeholder))
                                    .position(latLng)
                                    .zIndex(index)
                                    .title(title)
                            )
                            googleMarker.tag = tag
                        }
                    })
            } else {
                val googleMarker = googleMap.addMarker(
                    MarkerOptions()
                        .position(latLng)
                        .zIndex(index)
                        .title(title)
                )
                googleMarker.tag = tag
            }
        }

        val center = if (markers.size == 1) {
            LatLng(markers[0].lat, markers[0].lon)
        } else {
            DEFAULT_LATLNG
        }

        googleMap.setOnInfoWindowClickListener { googleMarker ->
            if (markers.size > 1) {
                if (googleMarker.tag is Marker) {
                    val marker = googleMarker.tag as Marker

                    if (marker.iceOffer != null) {
                        startActivity(PostActivity.getIntent(this, marker.iceOffer, marker.user))
                    } else {
                        startActivity(UserActivity.getIntent(this, marker.user.id))
                    }
                }
            }
        }
        googleMap.uiSettings.isZoomControlsEnabled = true
        googleMap.uiSettings.isCompassEnabled = true
        googleMap.setMinZoomPreference(6f)
        googleMap.setMaxZoomPreference(19f)
        googleMap.moveCamera(
            CameraUpdateFactory.newCameraPosition(
                CameraPosition.Builder()
                    .target(center)
                    .zoom(15f)
                    .build()
            )
        )

        try {
            disposable?.dispose()
            disposable = rxPermissions.requestEach(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
                .subscribe {
                    googleMap.isMyLocationEnabled = it.granted
                }
        } catch (e: SecurityException) {
            Log.e("MapActivity", e.message, e)
        }

        showProgress(false)
    }

    private fun showProgress(show: Boolean) {
        mapFragment?.view?.isVisible = !show
        progress_bar.isVisible = show
    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        return ContextCompat.getDrawable(context, vectorResId)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }

    override fun onDestroy() {
        disposable?.dispose()
        super.onDestroy()
    }

    companion object {
        private const val ARG_MARKERS = "map markers"

        fun getIntent(context: Context, markers: ArrayList<Marker>) = Intent(context, MapActivity::class.java).apply {
            putExtra(ARG_MARKERS, markers)
        }
    }
}