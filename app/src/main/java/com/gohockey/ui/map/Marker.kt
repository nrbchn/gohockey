package com.gohockey.ui.map

import android.os.Parcelable
import com.gohockey.model.IceOffer
import com.gohockey.model.User
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Marker(
    val lat: Double,
    val lon: Double,
    val user: User,
    val iceOffer: IceOffer? = null
) : Parcelable