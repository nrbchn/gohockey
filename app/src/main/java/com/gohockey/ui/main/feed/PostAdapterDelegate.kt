package com.gohockey.ui.main.feed

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gohockey.R
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import kotlinx.android.synthetic.main.item_post.view.*
import com.gohockey.model.Post
import com.gohockey.ui.addpost.AddPostActivity

class PostAdapterDelegate(private val clickListener: (Post) -> Unit = {}) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int): Boolean =
        items[position] is Post

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false))

    override fun onBindViewHolder(
        items: MutableList<Any>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {
        (holder as ViewHolder).bind(items[position] as Post)
    }

    private inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private lateinit var item: Post

        init {
            view.setOnClickListener { view.context.startActivity(Intent(view.context, AddPostActivity::class.java)) }
        }

        fun bind(item: Post) {
            this.item = item

            with(itemView) {
                Glide.with(this)
                    .load(item.user.photo)
                    .apply {
                        placeholder(R.drawable.ic_user_placeholder)
                        error(R.drawable.ic_user_placeholder)
                        circleCrop()
                    }
                    .into(post_user_image_view)

                post_user_name_text_view.text = item.user.name

                Glide.with(this)
                    .load(item.image)
                    .apply {
                        placeholder(android.R.color.darker_gray)
                        error(android.R.color.darker_gray)
                        centerCrop()
                    }
                    .into(post_image_view)

                var like = false
                post_likes_text_view.text = "${item.likes} отметок \"Нравится\""
                post_like_button.setOnClickListener {
                    like = !like
                    post_like_button.setImageResource(if (like) R.drawable.ic_like_filled else R.drawable.ic_like)
                    post_likes_text_view.text = "${ if (like) item.likes + 1 else item.likes} отметок \"Нравится\""
                }

                post_comment_button.setOnClickListener { context.startActivity(Intent(context, AddPostActivity::class.java)) }
                post_share_button.setOnClickListener { context.startActivity(Intent(context, AddPostActivity::class.java)) }

                post_title_text_view.text = item.title
                post_desc_text_view.text = item.desc
            }
        }
    }
}