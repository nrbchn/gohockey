package com.gohockey.ui.main.messages

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.gohockey.R
import com.gohockey.presentation.main.messages.MessagesPresenter
import com.gohockey.presentation.main.messages.MessagesView
import com.gohockey.ui.chat.ChatActivity
import com.gohockey.ui.global.BaseFragment
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import kotlinx.android.synthetic.main.fragment_messages.*

class MessagesFragment : BaseFragment(), MessagesView {

    override val layoutRes = R.layout.fragment_messages

    private val adapter by lazy { MessagesAdapter() }

    @InjectPresenter
    lateinit var presenter: MessagesPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.inflateMenu(R.menu.chat)
        toolbar.setOnMenuItemClickListener {
            if (it.itemId == R.id.write) {
                startActivity(ChatActivity.getIntent(context!!, null))
                return@setOnMenuItemClickListener true
            }
            false
        }
        swipe_refresh.setOnRefreshListener { presenter.refresh() }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(context)
            adapter = this@MessagesFragment.adapter
        }
    }

    override fun showData(data: List<Any>) {
        postViewAction { adapter.setData(data) }
    }

    override fun showError(message: String?) {
        error_text_view.text = message
        error_text_view.isVisible = !message.isNullOrEmpty()
    }

    override fun showProgress(show: Boolean) {
        swipe_refresh.isRefreshing = show
    }

    private inner class MessagesAdapter : ListDelegationAdapter<MutableList<Any>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(MessageAdapterDelegate { startActivity(ChatActivity.getIntent(context!!, it.user)) })
        }

        fun setData(data: List<Any>) {
            items.clear()
            items.addAll(data)
            notifyDataSetChanged()
        }
    }
}