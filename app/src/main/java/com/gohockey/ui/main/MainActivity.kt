package com.gohockey.ui.main

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.arellomobile.mvp.MvpAppCompatActivity
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.Prefs
import com.gohockey.ui.addpost.AddPostActivity
import com.gohockey.ui.global.BaseFragment
import com.gohockey.ui.main.addpost.AddPostFragment
import com.gohockey.ui.main.feed.FeedFragment
import com.gohockey.ui.main.messages.MessagesFragment
import com.gohockey.ui.main.profile.ProfileFragment
import com.gohockey.ui.main.search.SearchFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

private val tabKeys = arrayOf(
    "main feed tab",
    "main search tab",
    "main add post tab",
    "main messages tab",
    "main profile tab"
)

private val tabs = hashMapOf(
    tabKeys[0] to FeedFragment(),
    tabKeys[1] to SearchFragment(),
    tabKeys[2] to AddPostFragment(),
    tabKeys[3] to MessagesFragment(),
    tabKeys[4] to ProfileFragment()
)

class MainActivity : MvpAppCompatActivity() {
    companion object {
        fun getIntent(context: Context) = Intent(context, MainActivity::class.java)
    }

    private val currentFragment: BaseFragment?
        get() = supportFragmentManager.fragments.firstOrNull { !it.isHidden } as? BaseFragment

    private val prefs by lazy { Prefs(this) }
    private val notificationManager by lazy { getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager }

    private var messagesDisposable: Disposable? = null
    private var requestsDisposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FirebaseMessaging.getInstance().subscribeToTopic(FirebaseAuth.getInstance().currentUser?.uid)

        with(bottom_navigation) {
            isBehaviorTranslationEnabled = false
            isTranslucentNavigationEnabled = true
            titleState = AHBottomNavigation.TitleState.ALWAYS_SHOW

            addItem(AHBottomNavigationItem("Льды", R.drawable.ic_home_selector))
            addItem(AHBottomNavigationItem("Игроки", R.drawable.ic_search_selector))
            addItem(AHBottomNavigationItem("Создать", R.drawable.ic_add))
            addItem(AHBottomNavigationItem("Чаты", R.drawable.ic_chat_selector))
            addItem(AHBottomNavigationItem("Профиль", R.drawable.ic_profile_selector))
            isForceTint = false

            setOnTabSelectedListener { position, wasSelected ->
                if (position == 2) {
                    startActivity(Intent(this@MainActivity, AddPostActivity::class.java))
                    return@setOnTabSelectedListener false
                }

                if (position == 3) {
                    prefs.messages = 0
                    bottom_navigation.setNotification("", 3)
                }

                if (!wasSelected) showTab(position)
                true
            }
        }

        showTab(tabKeys.indexOf(currentFragment?.tag ?: tabKeys[0]))
    }

    private fun showTab(tabIndex: Int) {
        val newFragment = supportFragmentManager.findFragmentByTag(tabKeys[tabIndex])

        if (currentFragment != null && newFragment != null && currentFragment == newFragment) {
            return
        }

        supportFragmentManager.beginTransaction().apply {
            if (newFragment == null) add(R.id.content_layout, tabs[tabKeys[tabIndex]]!!, tabKeys[tabIndex])

            currentFragment?.let {
                hide(it)
                it.userVisibleHint = false
            }
            newFragment?.let {
                show(it)
                it.userVisibleHint = true
            }
        }.commitNow()
    }

    override fun onBackPressed() {
        when (currentFragment?.tag) {
            tabKeys[0] -> super.onBackPressed()
            else -> bottom_navigation.setCurrentItem(0, true)
        }
    }

    override fun onResume() {
        super.onResume()
        messagesDisposable = App.notificationController.messagesState
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { count ->
                    if (bottom_navigation.currentItem != 3) {
                        val title = if (count < 1) "" else count.toString()
                        bottom_navigation.setNotification(title, 3)
                    } else {
                        prefs.messages = 0
                        bottom_navigation.setNotification("", 3)
                    }
                },
                {
                    Log.e("MainActivity", it.message, it)
                }
            )
        requestsDisposable = App.notificationController.requestsState
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { count ->
                    val title = if (count < 1) "" else count.toString()
                    bottom_navigation.setNotification(title, 4)
                },
                {
                    Log.e("MainActivity", it.message, it)
                }
            )
    }

    override fun onPause() {
        messagesDisposable?.dispose()
        requestsDisposable?.dispose()
        super.onPause()
    }
}