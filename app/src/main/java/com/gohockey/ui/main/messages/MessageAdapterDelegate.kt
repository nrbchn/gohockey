package com.gohockey.ui.main.messages

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gohockey.R
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import kotlinx.android.synthetic.main.item_message.view.*
import com.gohockey.model.message.MessageItem

class MessageAdapterDelegate(private val clickListener: (MessageItem) -> Unit) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int): Boolean =
        items[position] is MessageItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_message, parent, false))

    override fun onBindViewHolder(
        items: MutableList<Any>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {
        (holder as ViewHolder).bind(items[position] as MessageItem, position == items.lastIndex)
    }

    private inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private lateinit var item: MessageItem

        init {
            view.setOnClickListener { clickListener(item) }
        }

        fun bind(item: MessageItem, last: Boolean) {
            this.item = item

            with(itemView) {
                Glide.with(this)
                    .load(item.user.photo)
                    .apply {
                        placeholder(R.drawable.ic_user_placeholder)
                        error(R.drawable.ic_user_placeholder)
                        circleCrop()
                    }
                    .into(message_user_image_view)

                message_user_text_view.text = item.user.name
                message_date_text_view.text = item.date
                message_text_view.text = item.text
                message_divider.visibility = if (last) View.GONE else View.VISIBLE
            }
        }
    }
}