package com.gohockey.ui.main.addpost

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.setPadding
import androidx.recyclerview.widget.GridLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.gohockey.R
import com.gohockey.presentation.main.addpost.AddPostPresenter
import com.gohockey.presentation.main.addpost.AddPostView
import com.gohockey.ui.addpost.AddPostActivity
import com.gohockey.ui.global.BaseFragment
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import kotlinx.android.synthetic.main.layout_base_list.*

class AddPostFragment : BaseFragment(), AddPostView {

    override val layoutRes = R.layout.fragment_add_post

    private val adapter by lazy { AddPostAdapter() }

    @InjectPresenter
    lateinit var presenter: AddPostPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        swipe_refresh.isEnabled = false

        with(recycler_view) {
            layoutManager = GridLayoutManager(context, 2)
            adapter = this@AddPostFragment.adapter
            setPadding(resources.getDimensionPixelOffset(R.dimen.card_margin))
            clipToPadding = false
        }
    }

    override fun showData(data: List<Any>) {
        postViewAction { adapter.setData(data) }
    }

    override fun showAddPostScreen() {
        startActivity(Intent(activity, AddPostActivity::class.java))
    }

    private inner class AddPostAdapter : ListDelegationAdapter<MutableList<Any>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(PostCategoryAdapterDelegate { presenter.onCategoryClicked(it) })
        }

        fun setData(data: List<Any>) {
            items.clear()
            items.addAll(data)
            notifyDataSetChanged()
        }
    }
}