package com.gohockey.ui.main.profile

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.Prefs
import com.gohockey.ui.global.BaseFragment
import com.gohockey.ui.login.LoginActivity
import com.gohockey.ui.myposts.MyPostsActivity
import com.gohockey.ui.participate.ParticipateActivity
import com.gohockey.ui.requests.RequestsActivity
import com.gohockey.ui.user.UserActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_profile

    private val currentUser = FirebaseAuth.getInstance()
    private val prefs by lazy { Prefs(context!!) }
    private var disposable: Disposable? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        my_info_button.setOnClickListener {
            startActivity(UserActivity.getIntent(context!!, currentUser.uid!!))
        }

        invites_button.setOnClickListener {
            startActivity(Intent(context!!, RequestsActivity::class.java))
        }

        participate_button.setOnClickListener {
            startActivity(Intent(context!!, ParticipateActivity::class.java))
        }

        my_activities_button.setOnClickListener {
            startActivity(Intent(context!!, MyPostsActivity::class.java))
        }

        logout_button.setOnClickListener {
            AlertDialog.Builder(context!!)
                .setMessage("Выйти из аккаунта?")
                .setPositiveButton("Выйти") { _, _ ->
                    prefs.messages = 0
                    prefs.requests = 0
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(FirebaseAuth.getInstance().currentUser?.uid)
                    FirebaseAuth.getInstance().signOut()
                    startActivity(LoginActivity.getIntent(context!!))
                    activity?.finish()
                }
                .setNegativeButton("Отмена", null)
                .show()
        }
    }

    override fun onResume() {
        super.onResume()
        disposable = App.notificationController.requestsState
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { count ->
                    requests_notification_text_view.text = count.toString()
                    requests_notification_text_view.isVisible = count > 0
                },
                {
                    Log.e("MainActivity", it.message, it)
                }
            )
    }

    override fun onPause() {
        disposable?.dispose()
        super.onPause()
    }
}