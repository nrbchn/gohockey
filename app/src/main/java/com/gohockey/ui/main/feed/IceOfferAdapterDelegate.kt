package com.gohockey.ui.main.feed

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.old.Metro
import com.gohockey.ui.map.Marker
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import kotlinx.android.synthetic.main.item_ice_offer.view.*
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

class IceOfferAdapterDelegate(
    private val clickListener: (IceOfferItem) -> Unit = {},
    private val locationClickListener: (Marker) -> Unit = {}
) : AdapterDelegate<MutableList<Any>>() {

    private val formatterDate = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
    private val formatterTime = SimpleDateFormat("HH:mm", Locale.getDefault())

    override fun isForViewType(items: MutableList<Any>, position: Int): Boolean =
        items[position] is IceOfferItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_ice_offer, parent, false))

    override fun onBindViewHolder(
        items: MutableList<Any>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {
        (holder as ViewHolder).bind(items[position] as IceOfferItem)
    }

    private inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private lateinit var item: IceOfferItem

        init {
            view.setOnClickListener { clickListener(item) }
            view.post_location_button.setOnClickListener {
                val metro = Metro.getMetroById(item.post.metroId)
                val marker = Marker(
                    lat = metro.lat,
                    lon = metro.lng,
                    user = item.author,
                    iceOffer = item.post
                )
                locationClickListener(marker)
            }
        }

        fun bind(item: IceOfferItem) {
            this.item = item

            with(itemView) {
                Glide.with(this)
                    .load(item.author.photo)
                    .apply {
                        placeholder(R.drawable.ic_user_placeholder)
                        error(R.drawable.ic_user_placeholder)
                        circleCrop()
                    }
                    .into(post_user_image_view)

                post_user_name_text_view.text = item.author.name


                Glide.with(this)
                    .load(when (item.post.theme) {
                        "Тренировка" -> R.drawable.img_training
                        "Тренировка и двухсторонка", "Тренировка+двухсторонка" -> R.drawable.img_training_double_side
                        "Двухсторонка" -> R.drawable.img_double_side
                        else -> URL("https://firebasestorage.googleapis.com/v0/b/hockeyapp-36361.appspot.com/o/Image%207-4-19%20at%2014.32.jpg?alt=media&token=8491b86d-5b7f-414c-9305-9c2cf508d887")
                    })
                    .apply {
                        placeholder(android.R.color.darker_gray)
                        error(android.R.color.darker_gray)
                        centerCrop()
                    }
                    .into(post_image_view)

                val metro = App.subways.find { it.id == item.post.metroId }?.name
                post_metro_text_view.visibility = if (metro.isNullOrEmpty()) View.GONE else View.VISIBLE
                val distance = String.format("%.1f", item.distance) ;
                post_metro_text_view.text = "$metro, $distance км"
                post_price_text_view.text = item.post.price + " \u20BD"
                post_title_text_view.text = item.post.nameIce
                post_theme_text_view.text = item.post.theme
                start_date_text.text = formatterDate.format(item.post.startDate.toDate())
                start_time_text.text = formatterTime.format(item.post.startDate.toDate())
            }
        }
    }
}