package com.gohockey.ui.main.feed

import com.gohockey.model.IceOffer
import com.gohockey.model.User

data class IceOfferItem(
    val author: User,
    val post: IceOffer,
    val distance: Double
)