package com.gohockey.ui.main.addpost

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gohockey.R
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import kotlinx.android.synthetic.main.item_post_category.view.*
import com.gohockey.model.PostCategory

class PostCategoryAdapterDelegate(private val clickListener: (PostCategory) -> Unit) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int): Boolean =
        items[position] is PostCategory

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_post_category, parent, false))

    override fun onBindViewHolder(
        items: MutableList<Any>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {
        (holder as ViewHolder).bind(items[position] as PostCategory)
    }

    private inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private lateinit var item: PostCategory

        init {
            view.setOnClickListener { clickListener(item) }
        }

        fun bind(item: PostCategory) {
            this.item = item

            with(itemView) {
                post_category_image_view.setImageResource(item.resId)
                post_category_text_view.text = item.name
            }
        }
    }
}