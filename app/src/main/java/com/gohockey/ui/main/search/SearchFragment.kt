package com.gohockey.ui.main.search

import android.os.Bundle
import android.view.View
import com.gohockey.R
import com.gohockey.model.member.MemberRole
import com.gohockey.ui.global.BaseFragment
import com.gohockey.ui.search.SearchActivity
import kotlinx.android.synthetic.main.fragment_search.*

class SearchFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_search

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        players_button.setOnClickListener { startActivity(SearchActivity.getIntent(context!!, MemberRole.PLAYER)) }
        keepers_button.setOnClickListener { startActivity(SearchActivity.getIntent(context!!, MemberRole.KEEPER)) }
    }
}