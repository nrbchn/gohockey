package com.gohockey.ui.main.feed

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.view.setPadding
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gohockey.R
import com.gohockey.model.Prefs
import com.gohockey.presentation.main.feed.FeedPresenter
import com.gohockey.presentation.main.feed.FeedView
import com.gohockey.ui.global.BaseFragment
import com.gohockey.ui.map.MapActivity
import com.gohockey.ui.map.Marker
import com.gohockey.ui.post.PostActivity
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import kotlinx.android.synthetic.main.fragment_feed.*
import kotlinx.android.synthetic.main.layout_base_list.*

class FeedFragment : BaseFragment(), FeedView {

    override val layoutRes = R.layout.fragment_feed

    private val prefs by lazy { Prefs(context!!) }
    private val fusedLocationClientProvider by lazy { LocationServices.getFusedLocationProviderClient(context!!) }
    private val adapter by lazy { FeedAdapter() }

    @InjectPresenter
    lateinit var presenter: FeedPresenter

    @ProvidePresenter
    fun providePresenter() = FeedPresenter(prefs, fusedLocationClientProvider)

    enum class Sort {
        DATE,
        START,
        PRICE,
        METRO
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.inflateMenu(R.menu.feed)
        toolbar.setOnMenuItemClickListener {
            when (it.itemId)  {
                R.id.map -> presenter.openMap()
                R.id.sort -> {
                    AlertDialog.Builder(context!!)
                        .setTitle("Сортировать по")
                        .setItems(arrayOf("Дате публикации", "Времени начала", "Стоимости", "Удаленности от метро")) { _, position ->
                            presenter.sort(
                                when (position) {
                                    0 -> Sort.DATE
                                    1 -> Sort.START
                                    2 -> Sort.PRICE
                                    else -> Sort.METRO
                                }
                            )
                        }
                        .show()
                }
            }
            true
        }

        swipe_refresh.setOnRefreshListener { presenter.refresh() }

        with(recycler_view) {
            layoutManager = LinearLayoutManager(context)
            adapter = this@FeedFragment.adapter
            setPadding(resources.getDimensionPixelOffset(R.dimen.card_margin))
            clipToPadding = false
        }
    }

    override fun showData(data: List<Any>) {
        adapter.setData(data)

        val maxLogSize = 1000
        val json = Gson().toJson(data)
        for (i in 0..json.length / maxLogSize) {
            val start = i * maxLogSize
            var end = (i + 1) * maxLogSize
            end = if (end > json.length) json.length else end
            Log.d("FeedFragment", json.substring(start, end))
        }
    }

    override fun showProgress(show: Boolean) {
        swipe_refresh.isRefreshing = show
    }

    override fun showError(message: String?) {
        error_text_view.visibility = if (message.isNullOrEmpty()) View.GONE else View.VISIBLE
        error_text_view.text = message
    }

    override fun onResume() {
        super.onResume()
        presenter.refresh()
    }

    override fun showMap(markers: List<Marker>) {
        if (markers.isNotEmpty()) {
            startActivity(MapActivity.getIntent(context!!, ArrayList(markers)))
        }
    }

    private inner class FeedAdapter : ListDelegationAdapter<MutableList<Any>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(IceOfferAdapterDelegate(
                { startActivity(PostActivity.getIntent(context!!, it.post, it.author)) },
                { startActivity(MapActivity.getIntent(context!!, ArrayList(listOf(it)))) }
            ))
        }

        fun setData(data: List<Any>) {
            items.clear()
            items.addAll(data)
            notifyDataSetChanged()
        }
    }
}