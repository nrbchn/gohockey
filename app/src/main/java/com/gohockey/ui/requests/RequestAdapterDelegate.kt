package com.gohockey.ui.requests

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gohockey.R
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import kotlinx.android.synthetic.main.item_request.view.*

class RequestAdapterDelegate(
    private val clickListener: (RequestItem) -> Unit = {}
) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int): Boolean =
        items[position] is RequestItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_request, parent, false))

    override fun onBindViewHolder(
        items: MutableList<Any>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {
        (holder as ViewHolder).bind(items[position] as RequestItem, position == items.lastIndex)
    }

    private inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        lateinit var item: RequestItem

        init {
            view.setOnClickListener { clickListener(item) }
        }

        fun bind(item: RequestItem, last: Boolean) {
            this.item = item

            with(itemView) {
                Glide.with(this)
                    .load(item.user.photo)
                    .apply {
                        placeholder(R.drawable.ic_user_placeholder)
                        error(R.drawable.ic_user_placeholder)
                        circleCrop()
                    }
                    .into(request_user_image_view)

                request_user_text_view.text = item.user.name
                request_title_text_view.text = item.iceOffer.nameIce
                divider.isVisible = !last
            }
        }
    }
}