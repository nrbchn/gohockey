package com.gohockey.ui.requests

import com.gohockey.model.IceOffer
import com.gohockey.model.User

data class RequestItem(
    val user: User,
    val iceOffer: IceOffer
)