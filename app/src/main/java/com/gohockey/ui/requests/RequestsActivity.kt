package com.gohockey.ui.requests

import android.os.Bundle
import android.util.Log
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.Prefs
import com.gohockey.presentation.requests.RequestsPresenter
import com.gohockey.presentation.requests.RequestsView
import com.gohockey.ui.user.UserActivity
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import kotlinx.android.synthetic.main.activity_requests.*

class RequestsActivity : MvpAppCompatActivity(), RequestsView {

    private val adapter by lazy { RequestsAdapter() }
    private val prefs by lazy { Prefs(this) }

    @InjectPresenter
    lateinit var presenter: RequestsPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_requests)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        swipe_refresh.setOnRefreshListener { presenter.refresh() }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(context)
            adapter = this@RequestsActivity.adapter
        }
        prefs.requests = 0
        App.notificationController.setRequests(0)
    }

    override fun showRequests(requests: List<RequestItem>) {
        Log.d("RequestsActivity", requests.toString())
        adapter.setData(requests)
    }

    override fun showError(message: String?) {
        error_text_view.text = message
        error_text_view.isVisible = !message.isNullOrEmpty()
    }

    override fun showProgress(show: Boolean) {
        swipe_refresh.isRefreshing = show
    }

    override fun onResume() {
        super.onResume()
        presenter.refresh()
    }

    private inner class RequestsAdapter : ListDelegationAdapter<MutableList<Any>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(RequestAdapterDelegate {
                startActivity(UserActivity.getIntent(this@RequestsActivity, it.user.id, it.iceOffer))
            })
        }

        fun setData(data: List<Any>) {
            items.clear()
            items.addAll(data)
            notifyDataSetChanged()
        }
    }
}