package com.gohockey.ui.edit.profile

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.arellomobile.mvp.MvpAppCompatActivity
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.member.MemberRole
import com.gohockey.model.old.Admin
import com.gohockey.model.old.GoalKeeper
import com.gohockey.model.old.Player
import com.gohockey.model.subway.SubwayStation
import com.gohockey.ui.global.BaseFragment
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.Command

class EditActivity : MvpAppCompatActivity() {
    companion object {
        private const val ARG_USER = "profile user"
        const val NO_LEVEL = "Квалификация *"
        const val NO_GRIP = "Хват *"
        const val NO_LINE = "Амплуа *"
        const val NO_SUBWAY = "Ближайшая станция метро *"

        var player: Player? = null
            private set
        var goalKeeper: GoalKeeper? = null
            private set
        var admin: Admin? = null
            private set

        var avatarUri: Uri? = null
        var level = NO_LEVEL
        var grip = NO_GRIP
        var line = NO_LINE
        var subway: SubwayStation? = null
        var priceStart = 0f
        var priceEnd = 0f

        val role get() = if (player != null) {
            MemberRole.PLAYER
        } else if (goalKeeper != null) {
            MemberRole.KEEPER
        } else {
            MemberRole.ADMIN
        }

        fun setUser(player: Player) {
            this.player = player
            avatarUri = try { Uri.parse(player.avatarUrl) } catch (e: Exception) { null }
            level = player.qualification
            grip = when (player.handSide) {
                1 -> "Правый"
                0 -> "Левый"
                else -> NO_GRIP
            }
            line = player.role
            subway = App.subways.find { it.id == player.metroId }
            priceStart = 0f
            priceEnd = 0f
        }

        fun setUser(goalKeeper: GoalKeeper) {
            this.goalKeeper = goalKeeper
            avatarUri = try { Uri.parse(goalKeeper.avatarUrl) } catch (e: Exception) { null }
            level = goalKeeper.qualification
            grip = when (goalKeeper.handSide) {
                1 -> "Правый"
                0 -> "Левый"
                else -> NO_GRIP
            }
            line = NO_LINE
            subway = App.subways.find { it.id == goalKeeper.metroId }
            priceStart = try { goalKeeper.priceFrom.toFloat() } catch (e: Exception) { 0f }
            priceEnd = try { goalKeeper.priceTo.toFloat() } catch (e: Exception) { 0f }
        }

        fun setUser(admin: Admin) {
            this.admin = admin
            avatarUri = try { Uri.parse(admin.avatarUrl) } catch (e: Exception) { null }
            level = NO_LEVEL
            grip = NO_GRIP
            line = NO_LINE
            subway = App.subways.find { it.id == admin.metroId }
            priceStart = 0f
            priceEnd = 0f
        }

        fun clear() {
            player = null
            goalKeeper = null
            admin = null
        }
    }

    private val currentFragment: BaseFragment?
        get() = supportFragmentManager.findFragmentById(R.id.content_layout) as? BaseFragment

    private val navigator: Navigator =
        object : SupportAppNavigator(this, supportFragmentManager, R.id.content_layout) {
            override fun setupFragmentTransaction(
                command: Command?,
                currentFragment: Fragment?,
                nextFragment: Fragment?,
                fragmentTransaction: FragmentTransaction
            ) {
                fragmentTransaction.setReorderingAllowed(true)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_container)

        if (savedInstanceState == null) {
            App.router.newRootScreen(Screens.Edit)
        }
    }

    override fun onResume() {
        super.onResume()
        App.navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        App.navigatorHolder.removeNavigator()
    }

    override fun onBackPressed() {
        currentFragment?.onBackPressed() ?: super.onBackPressed()
    }
}

object Screens {
    object Edit : SupportAppScreen() {
        override fun getFragment() = EditFragment()
    }
    object Level : SupportAppScreen() {
        override fun getFragment() = LevelFragment()
    }
    object Grip : SupportAppScreen() {
        override fun getFragment() = GripFragment()
    }
    object Line : SupportAppScreen() {
        override fun getFragment() = LineFragment()
    }
    object Subway : SupportAppScreen() {
        override fun getFragment() = SubwayFragment()
    }
    object Price : SupportAppScreen() {
        override fun getFragment() = PriceFragment()
    }
}