package com.gohockey.ui.edit.profile

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.FirestoreCollections
import com.gohockey.model.member.MemberRole
import com.gohockey.ui.global.BaseFragment
import com.gohockey.ui.global.ProgressDialog
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_register_profile.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import kotlin.math.roundToInt

class EditFragment : BaseFragment() {

    override val layoutRes = R.layout.fragment_register_profile

    private val currentUser = FirebaseAuth.getInstance().currentUser
    private val rxPermissions by lazy { RxPermissions(this) }
    private var disposable: Disposable? = null
    private var outputFileUri: Uri? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbar_title.text = "Редактировать"
        level_button.setOnClickListener { App.router.navigateTo(Screens.Level) }
        grip_button.setOnClickListener { App.router.navigateTo(Screens.Grip) }
        line_button.setOnClickListener { App.router.navigateTo(Screens.Line) }
        subway_button.setOnClickListener { App.router.navigateTo(Screens.Subway) }
        price_button.setOnClickListener { App.router.navigateTo(Screens.Price) }
        if (savedInstanceState == null) {
            name_edit_text.setText(
                when (EditActivity.role) {
                    MemberRole.PLAYER -> EditActivity.player?.name ?: ""
                    MemberRole.KEEPER -> EditActivity.goalKeeper?.name ?: ""
                    MemberRole.ADMIN -> EditActivity.admin?.name ?: ""
                }
            )
            name_edit_text.setSelection(name_edit_text.text.length)
            surname_edit_text.setText(
                when (EditActivity.role) {
                    MemberRole.PLAYER -> EditActivity.player?.surname ?: ""
                    MemberRole.KEEPER -> EditActivity.goalKeeper?.surname ?: ""
                    MemberRole.ADMIN -> EditActivity.admin?.surname ?: ""
                }
            )
            surname_edit_text.setSelection(surname_edit_text.text.length)
        }
        if (savedInstanceState == null) {
            age_edit_text.setText(
                when (EditActivity.role) {
                    MemberRole.PLAYER -> EditActivity.player?.age ?: ""
                    MemberRole.KEEPER -> EditActivity.goalKeeper?.age ?: ""
                    else -> ""
                }
            )
            age_edit_text.setSelection(age_edit_text.text.length)
        }
        age_edit_text.isVisible = EditActivity.role != MemberRole.ADMIN
        if (savedInstanceState == null) {
            keeper_switch.isChecked = EditActivity.goalKeeper?.isVisible ?: false
        }
        keeper_switch.isVisible = EditActivity.role == MemberRole.KEEPER
        if (savedInstanceState == null) {
            phone_edit_text.setText(
                when (EditActivity.role) {
                    MemberRole.ADMIN -> EditActivity.admin?.telephone ?: ""
                    else -> ""
                }
            )
            phone_edit_text.setSelection(phone_edit_text.text.length)
        }
        phone_edit_text.isVisible = EditActivity.role == MemberRole.ADMIN
        level_button.isVisible = EditActivity.role != MemberRole.ADMIN
        if (savedInstanceState == null) {
            height_edit_text.setText(
                when (EditActivity.role) {
                    MemberRole.PLAYER -> EditActivity.player?.height ?: ""
                    MemberRole.KEEPER -> EditActivity.goalKeeper?.height ?: ""
                    else -> ""
                }
            )
            height_edit_text.setSelection(height_edit_text.text.length)
        }
        height_edit_text.isVisible = EditActivity.role != MemberRole.ADMIN
        if (savedInstanceState == null) {
            weight_edit_text.setText(
                when (EditActivity.role) {
                    MemberRole.PLAYER -> EditActivity.player?.weight ?: ""
                    MemberRole.KEEPER -> EditActivity.goalKeeper?.weight ?: ""
                    else -> ""
                }
            )
            weight_edit_text.setSelection(weight_edit_text.text.length)
        }
        weight_edit_text.isVisible = EditActivity.role != MemberRole.ADMIN
        grip_button.isVisible = EditActivity.role != MemberRole.ADMIN
        line_button.isVisible = EditActivity.role == MemberRole.PLAYER
        price_button.isVisible = EditActivity.role == MemberRole.KEEPER
        if (savedInstanceState == null) {
            info_edit_text.setText(
                when (EditActivity.role) {
                    MemberRole.ADMIN -> EditActivity.admin?.additaionInformation ?: ""
                    else -> ""
                }
            )
            info_edit_text.setSelection(info_edit_text.text.length)
        }
        info_edit_text.isVisible = EditActivity.role == MemberRole.ADMIN
        add_image_button.setOnClickListener {
            AlertDialog.Builder(context!!)
                .setTitle("Загрузить фото")
                .setItems(arrayOf("Сделать фотографию", "Выбрать из галереи")) { _, position ->
                    when (position) {
                        0 -> {
                            disposable?.dispose()
                            disposable = rxPermissions.requestEach(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                .subscribe {
                                    if (it.granted) {
                                        openCamera()
                                    }
                                }
                        }
                        1 -> {
                            disposable?.dispose()
                            disposable = rxPermissions.requestEach(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                .subscribe {
                                    if (it.granted) {
                                        pickFile()
                                    }
                                }
                        }
                    }
                }
                .show()
        }
        done_button.text = "Сохранить"
        done_button.setOnClickListener {
            if (currentUser != null) {
                when (EditActivity.role) {
                    MemberRole.PLAYER -> {
                        if (name_edit_text.text.toString().trim().isEmpty() ||
                            surname_edit_text.text.toString().trim().isEmpty() ||
                            age_edit_text.text.toString().trim().isEmpty() ||
                            height_edit_text.text.toString().trim().isEmpty() ||
                            weight_edit_text.text.toString().trim().isEmpty() ||
                            EditActivity.level == EditActivity.NO_LEVEL ||
                            EditActivity.grip == EditActivity.NO_GRIP ||
                            EditActivity.line == EditActivity.NO_LINE ||
                            EditActivity.subway == null
                        ) {

                            showAlert(message = "Заполните все поля")
                        } else {
                            if (EditActivity.avatarUri == null) {
                                showAlert(message = "Загрузите фотографию")
                                return@setOnClickListener
                            }

                            showProgress(true)
                            val userId = currentUser.uid
                            val db = FirebaseFirestore.getInstance()

                            image_view.isDrawingCacheEnabled = true
                            image_view.buildDrawingCache()
                            val bitmap = (image_view.drawable as BitmapDrawable).bitmap
                            val baos = ByteArrayOutputStream()
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                            val data = baos.toByteArray()

                            val ref = FirebaseStorage.getInstance().reference.child("profiles/${userId}")
                            ref.putBytes(data)
                                .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                                    if (!task.isSuccessful) {
                                        task.exception?.let {
                                            throw it
                                        }
                                    }
                                    return@Continuation ref.downloadUrl
                                }).addOnCompleteListener { task ->
                                    if (task.isSuccessful) {
                                        val downloadUri = task.result
                                        val avatarUrl = downloadUri.toString()

                                        db.collection(FirestoreCollections.PLAYER)
                                            .document(userId)
                                            .set(
                                                hashMapOf(
                                                    "age" to age_edit_text.text.toString().trim(),
                                                    "avatarUrl" to avatarUrl,
                                                    "createdAt" to FieldValue.serverTimestamp(),
                                                    "find" to "",
                                                    "handSide" to when (EditActivity.grip) {
                                                        "Правый" -> 1
                                                        "Левый" -> 0
                                                        else -> 1
                                                    },
                                                    "height" to height_edit_text.text.toString().trim(),
                                                    "weight" to weight_edit_text.text.toString().trim(),
                                                    "idRole" to EditActivity.role!!.desc,
                                                    "metroId" to EditActivity.subway!!.id,
                                                    "qualification" to EditActivity.level,
                                                    "role" to EditActivity.line,
                                                    "name" to name_edit_text.text.toString().trim(),
                                                    "surname" to surname_edit_text.text.toString().trim(),
                                                    "userId" to userId
                                                )
                                            )
                                            .addOnSuccessListener {
                                                clearFields()
                                                activity?.finish()
                                            }
                                            .addOnFailureListener {
                                                showAlert(message = it.localizedMessage)
                                            }
                                            .addOnCompleteListener { showProgress(false) }
                                    } else {
                                        showProgress(false)
                                    }
                                }
                        }
                    }
                    MemberRole.KEEPER -> {
                        if (name_edit_text.text.toString().trim().isEmpty() ||
                            surname_edit_text.text.toString().trim().isEmpty() ||
                            age_edit_text.text.toString().trim().isEmpty() ||
                            height_edit_text.text.toString().trim().isEmpty() ||
                            weight_edit_text.text.toString().trim().isEmpty() ||
                            EditActivity.level == EditActivity.NO_LEVEL ||
                            EditActivity.grip == EditActivity.NO_GRIP ||
                            EditActivity.subway == null
                        ) {

                            showAlert(message = "Заполните все поля")
                        } else {
                            if (EditActivity.avatarUri == null) {
                                showAlert(message = "Загрузите фотографию")
                                return@setOnClickListener
                            }

                            showProgress(true)
                            val userId = currentUser.uid
                            val db = FirebaseFirestore.getInstance()

                            image_view.isDrawingCacheEnabled = true
                            image_view.buildDrawingCache()
                            val bitmap = (image_view.drawable as BitmapDrawable).bitmap
                            val baos = ByteArrayOutputStream()
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                            val data = baos.toByteArray()

                            val ref = FirebaseStorage.getInstance().reference.child("profiles/$userId")
                            ref.putBytes(data)
                                .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                                    if (!task.isSuccessful) {
                                        task.exception?.let {
                                            throw it
                                        }
                                    }
                                    return@Continuation ref.downloadUrl
                                }).addOnCompleteListener { task ->
                                    if (task.isSuccessful) {
                                        val downloadUri = task.result
                                        val avatarUrl = downloadUri.toString()

                                        db.collection(FirestoreCollections.KEEPER)
                                            .document(userId)
                                            .set(
                                                hashMapOf(
                                                    "age" to age_edit_text.text.toString().trim(),
                                                    "avatarUrl" to avatarUrl,
                                                    "createdAt" to FieldValue.serverTimestamp(),
                                                    "handSide" to when (EditActivity.grip) {
                                                        "Правый" -> 1
                                                        "Левый" -> 0
                                                        else -> 1
                                                    },
                                                    "height" to height_edit_text.text.toString().trim(),
                                                    "idRole" to EditActivity.role!!.desc,
                                                    "isVisible" to keeper_switch.isChecked,
                                                    "metroId" to EditActivity.subway!!.id,
                                                    "name" to name_edit_text.text.toString().trim(),
                                                    "priceFrom" to EditActivity.priceStart.roundToInt().toString(),
                                                    "priceTo" to EditActivity.priceEnd.roundToInt().toString(),
                                                    "qualification" to EditActivity.level,
                                                    "surname" to surname_edit_text.text.toString().trim(),
                                                    "userId" to userId,
                                                    "weight" to weight_edit_text.text.toString().trim()
                                                )
                                            )
                                            .addOnSuccessListener {
                                                clearFields()
                                                activity?.finish()
                                            }
                                            .addOnFailureListener {
                                                showAlert(message = it.localizedMessage)
                                            }
                                            .addOnCompleteListener { showProgress(false) }
                                    } else {
                                        showProgress(false)
                                    }
                                }
                        }
                    }
                    MemberRole.ADMIN -> {
                        if (name_edit_text.text.toString().trim().isEmpty() ||
                            surname_edit_text.text.toString().trim().isEmpty() ||
                            phone_edit_text.text.toString().trim().isEmpty() ||
                            EditActivity.subway == null
                        ) {

                            showAlert(message = "Заполните все поля")
                        } else {
                            if (EditActivity.avatarUri == null) {
                                showAlert(message = "Загрузите фотографию")
                                return@setOnClickListener
                            }

                            showProgress(true)
                            val userId = currentUser.uid
                            val db = FirebaseFirestore.getInstance()

                            image_view.isDrawingCacheEnabled = true
                            image_view.buildDrawingCache()
                            val bitmap = (image_view.drawable as BitmapDrawable).bitmap
                            val baos = ByteArrayOutputStream()
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                            val data = baos.toByteArray()

                            val ref = FirebaseStorage.getInstance().reference.child("profiles/${userId}")
                            ref.putBytes(data)
                                .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                                    if (!task.isSuccessful) {
                                        task.exception?.let {
                                            throw it
                                        }
                                    }
                                    return@Continuation ref.downloadUrl
                                })
                                .addOnCompleteListener { task ->
                                    if (task.isSuccessful) {
                                        val downloadUri = task.result
                                        val avatarUrl = downloadUri.toString()

                                        db.collection(FirestoreCollections.ADMIN)
                                            .document(userId)
                                            .set(
                                                hashMapOf(
                                                    "additaionInformation" to info_edit_text.text.toString().trim(),
                                                    "avatarUrl" to avatarUrl,
                                                    "createdAt" to FieldValue.serverTimestamp(),
                                                    "idRole" to EditActivity.role!!.desc,
                                                    "metroId" to EditActivity.subway!!.id,
                                                    "name" to name_edit_text.text.toString().trim(),
                                                    "surname" to surname_edit_text.text.toString().trim(),
                                                    "teamId" to null,
                                                    "telephone" to phone_edit_text.text.toString().trim(),
                                                    "userId" to userId,
                                                    "work" to ""
                                                )
                                            )
                                            .addOnSuccessListener {
                                                clearFields()
                                                activity?.finish()
                                            }
                                            .addOnFailureListener {
                                                showAlert(message = it.localizedMessage)
                                            }
                                            .addOnCompleteListener { showProgress(false) }
                                    } else {
                                        showProgress(false)
                                    }
                                }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        level_button.text = EditActivity.level
        grip_button.text = EditActivity.grip
        line_button.text = EditActivity.line
        subway_button.text = EditActivity.subway?.name ?: EditActivity.NO_SUBWAY
        Glide.with(this)
            .load(EditActivity.avatarUri)
            .apply {
                placeholder(R.drawable.ic_user_placeholder_60)
                error(R.drawable.ic_user_placeholder_60)
                circleCrop()
            }
            .into(image_view)
    }

    private fun showAlert(title: String? = null, message: String) {
        AlertDialog.Builder(context!!)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("Ок", null)
            .show()
    }

    private fun showProgress(show: Boolean) {
        if (!isAdded) return

        val fragment = childFragmentManager.findFragmentByTag("progress dialog tag")
        if (fragment != null && !show) {
            (fragment as ProgressDialog).dismissAllowingStateLoss()
            childFragmentManager.executePendingTransactions()
        } else if (fragment == null && show) {
            ProgressDialog().show(childFragmentManager, "progress dialog tag")
            childFragmentManager.executePendingTransactions()
        }
    }

    private fun clearFields() {
        EditActivity.clear()
        EditActivity.level = EditActivity.NO_LEVEL
        EditActivity.grip = EditActivity.NO_GRIP
        EditActivity.line = EditActivity.NO_LINE
        EditActivity.subway = null
        EditActivity.priceStart = 0f
        EditActivity.priceEnd = 0f
        EditActivity.avatarUri = null
    }

    override fun onBackPressed() {
        clearFields()
        App.router.exit()
    }

    private fun pickFile() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        try {
            startActivityForResult(
                Intent.createChooser(intent, "Выбрать "),
                FILE_SELECT_CODE
            )
        } catch (e: Exception) {
            Log.e("ProfileFragment", e.message, e)
        }
    }

    private fun openCamera() {
        val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePhotoIntent.resolveActivity(context!!.packageManager) != null) {
            val photoFile = try {
                val storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                storageDirectory.mkdir()
                File.createTempFile("IMAGE_${System.currentTimeMillis()}", ".jpg", storageDirectory)
            } catch (e: IOException) {
                Log.e("ProfileFragment", e.message, e)
                null
            }
            photoFile?.let {
                outputFileUri = FileProvider.getUriForFile(context!!, "${context!!.applicationContext!!.packageName}.provider", it)
                takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)
                startActivityForResult(takePhotoIntent, CAMERA_REQUEST_CODE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        when (requestCode) {
            FILE_SELECT_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    intent?.data?.let {
                        EditActivity.avatarUri = it
                    }
                }
            }
            CAMERA_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    outputFileUri?.let {
                        EditActivity.avatarUri = it
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        disposable?.dispose()
        super.onDestroyView()
    }

    companion object {
        private const val CAMERA_REQUEST_CODE = 101
        private const val FILE_SELECT_CODE = 185
    }
}