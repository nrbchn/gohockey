package com.gohockey.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gohockey.R
import com.gohockey.model.old.Metro
import com.gohockey.model.user.User
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import kotlinx.android.synthetic.main.item_goalkeeper.view.*

class GoalkeeperAdapterDelegate(
    private val clickListener: (User.Goalkeeper) -> Unit = {}
) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int): Boolean =
        items[position] is User.Goalkeeper

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_goalkeeper, parent, false))

    override fun onBindViewHolder(
        items: MutableList<Any>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {
        (holder as ViewHolder).bind(items[position] as User.Goalkeeper, position == items.lastIndex)
    }

    private inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        lateinit var item: User.Goalkeeper

        init {
            view.setOnClickListener { clickListener(item) }
        }

        fun bind(item: User.Goalkeeper, last: Boolean) {
            this.item = item

            with(itemView) {
                Glide.with(this)
                    .load(item.avatarUrl)
                    .apply {
                        placeholder(R.drawable.ic_user_placeholder)
                        error(R.drawable.ic_user_placeholder)
                        circleCrop()
                    }
                    .into(user_image_view)

                user_name_text_view.text = item.name + " " + item.surname
                user_age_text_view.text = if (item.age == 0) "не указан" else resources.getQuantityString(R.plurals.age, item.age, item.age)
                user_grip_text_view.text = item.handSide.title
                user_price_text_view.text = "от ${item.priceFrom} \u20BD до ${item.priceTo} \u20BD"
                user_metro_text_view.text = Metro.getNameById(item.metroId)
                user_divider.isVisible = !last
            }
        }
    }
}