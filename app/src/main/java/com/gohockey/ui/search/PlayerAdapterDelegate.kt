package com.gohockey.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gohockey.R
import com.gohockey.model.user.User
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import kotlinx.android.synthetic.main.item_user.view.*

class PlayerAdapterDelegate(
    private val clickListener: (User.Player) -> Unit = {}
) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int): Boolean =
        items[position] is User.Player

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false))

    override fun onBindViewHolder(
        items: MutableList<Any>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {
        (holder as ViewHolder).bind(items[position] as User.Player, position == items.lastIndex)
    }

    private inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        lateinit var item: User.Player

        init {
            view.setOnClickListener { clickListener(item) }
        }

        fun bind(item: User.Player, last: Boolean) {
            this.item = item

            with(itemView) {
                Glide.with(this)
                    .load(item.avatarUrl)
                    .apply {
                        placeholder(R.drawable.ic_user_placeholder)
                        error(R.drawable.ic_user_placeholder)
                        circleCrop()
                    }
                    .into(user_image_view)

                user_text_view.text = item.name + " " + item.surname
                user_divider.isVisible = !last
            }
        }
    }
}