package com.gohockey.ui.search

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gohockey.R
import com.gohockey.model.member.MemberRole
import com.gohockey.presentation.search.*
import com.gohockey.ui.map.MapActivity
import com.gohockey.ui.map.Marker
import com.gohockey.ui.user.UserActivity
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import kotlinx.android.synthetic.main.activity_search.*

class SearchActivity : MvpAppCompatActivity(), SearchView {

    private val memberRole get() = MemberRole.valueOf(intent.extras!!.getString(ARG_ROLE)!!)

    private val adapter by lazy { UsersAdapter() }

    @InjectPresenter
    lateinit var presenter: SearchPresenter

    @ProvidePresenter
    fun providePresenter() = SearchPresenter(memberRole)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        toolbar.setNavigationOnClickListener { onBackPressed() }
        toolbar.inflateMenu(R.menu.search)
        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.map -> presenter.openMap()
            }
            true
        }

        swipe_refresh.setOnRefreshListener { presenter.refresh() }

        with(recycler_view) {
            layoutManager = LinearLayoutManager(context)
            adapter = this@SearchActivity.adapter
        }
    }

    override fun showTitle(title: String) {
        toolbar_title.text = title
    }

    override fun showData(data: List<Any>) {
        adapter.setData(data)
    }

    override fun showError(message: String?) {
        error_text_view.text = message
        error_text_view.isVisible = !message.isNullOrEmpty()
    }

    override fun showProgress(show: Boolean) {
        swipe_refresh.isRefreshing = show
    }

    override fun showMap(markers: List<Marker>) {
        startActivity(MapActivity.getIntent(this, ArrayList(markers)))
    }

    private inner class UsersAdapter : ListDelegationAdapter<MutableList<Any>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(PlayerAdapterDelegate { startActivity(UserActivity.getIntent(this@SearchActivity, it.id)) })
            delegatesManager.addDelegate(GoalkeeperAdapterDelegate { startActivity(UserActivity.getIntent(this@SearchActivity, it.id)) })
        }

        fun setData(data: List<Any>) {
            items.clear()
            items.addAll(data)
            notifyDataSetChanged()
        }
    }

    companion object {
        private const val ARG_ROLE = "search activity role"

        fun getIntent(context: Context, memberRole: MemberRole) = Intent(context, SearchActivity::class.java).apply {
            putExtra(ARG_ROLE, memberRole.name)
        }
    }
}