package com.gohockey.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.gohockey.R
import com.gohockey.presentation.login.LoginPresenter
import com.gohockey.presentation.login.LoginView
import com.gohockey.ui.global.ProgressDialog
import com.gohockey.ui.main.MainActivity
import com.gohockey.ui.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.dialog_forgot_password.view.*

class LoginActivity : MvpAppCompatActivity(), LoginView {
    companion object {
        fun getIntent(context: Context) = Intent(context, LoginActivity::class.java)
    }

    @InjectPresenter
    lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        login_button.setOnClickListener {
            presenter.login(
                login_edit_text.text.toString().trim(),
                password_edit_text.text.toString().trim()
            )
        }
        forgot_button.setOnClickListener { showForgotPassword() }
        register_button.setOnClickListener { showRegisterScreen() }
    }

    override fun showMainScreen() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun showForgotPassword() {
        val view = LayoutInflater.from(this).inflate(R.layout.dialog_forgot_password, null)
        AlertDialog.Builder(this)
            .setTitle("Введите данные")
            .setView(view)
            .setPositiveButton("Ок") { _, _ ->
                presenter.sendNewPassword(view.forgot_edit_text.text.toString().trim())
            }
            .show()
    }

    private fun showRegisterScreen() {
        startActivity(Intent(this, RegisterActivity::class.java))
    }

    override fun showAlert(title: String, message: String) {
        AlertDialog.Builder(this)
            .setTitle(title.ifEmpty { null })
            .setMessage(message)
            .setPositiveButton("Ок", null)
            .show()
    }

    override fun showProgress(show: Boolean) {
        val fragment = supportFragmentManager.findFragmentByTag("progress tag")
        if (fragment != null && !show) {
            (fragment as ProgressDialog).dismissAllowingStateLoss()
            supportFragmentManager.executePendingTransactions()
        } else if (fragment == null && show) {
            ProgressDialog().show(supportFragmentManager, "progress tag")
            supportFragmentManager.executePendingTransactions()
        }
    }
}