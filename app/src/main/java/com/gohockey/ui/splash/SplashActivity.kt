package com.gohockey.ui.splash

import android.Manifest
import android.os.Bundle
import android.os.Handler
import com.arellomobile.mvp.MvpAppCompatActivity
import android.view.animation.DecelerateInterpolator
import android.animation.ObjectAnimator
import android.content.Intent
import android.view.animation.LinearInterpolator
import com.gohockey.App
import com.gohockey.R
import com.gohockey.ui.login.LoginActivity
import com.gohockey.ui.main.MainActivity
import com.gohockey.ui.register.RegisterFragment
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.JsonObject
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_splash.*
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

class SplashActivity : MvpAppCompatActivity() {

    private var handler = Handler()
    private val rxPermissions by lazy { RxPermissions(this) }
    private val fusedLocationClientProvider by lazy { LocationServices.getFusedLocationProviderClient(this) }
    private val currentUser = FirebaseAuth.getInstance().currentUser
    private var disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val animator = ObjectAnimator.ofInt(progress_bar, "progress", 0, progress_bar.max)
        animator.duration = 5000
        animator.interpolator = LinearInterpolator()
        animator.start()

        Retrofit.Builder()
            .baseUrl("http://michil.ru")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(Splash::class.java)
            .init()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorReturnItem(
                JsonObject().apply {
                    addProperty("isPizdaboled", false)
                }
            )
            .subscribe({
                if (it.get("isPizdaboled").asBoolean) {
                    startActivity(Intent(this, RegisterFragment::class.java))
                    finish()
                } else {
                    rxPermissions.request(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    )
                        .subscribe { granted ->
                            if (granted) {
                                getCurrentLocation()
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .onErrorReturnItem(App.currentLocation)
                                    .subscribe({
                                        App.currentLocation = it
                                        val progress = (animator.currentPlayTime / animator.duration * progress_bar.max).toInt()
                                        animator.end()
                                        if (progress == progress_bar.max) {
                                            if (currentUser != null) {
                                                startActivity(MainActivity.getIntent(this))
                                                finish()
                                            } else {
                                                startActivity(LoginActivity.getIntent(this))
                                                finish()
                                            }
                                        } else {
                                            val delay = 1000L
                                            ObjectAnimator.ofInt(progress_bar, "progress", progress, progress_bar.max)
                                                .apply {
                                                    duration = delay
                                                    interpolator = DecelerateInterpolator()
                                                }
                                                .start()

                                            handler.postDelayed({
                                                if (currentUser != null) {
                                                    startActivity(MainActivity.getIntent(this))
                                                    finish()
                                                } else {
                                                    startActivity(LoginActivity.getIntent(this))
                                                    finish()
                                                }
                                            }, delay)
                                        }
                                    }, {})
                                    .connect()

                            } else {
                                val progress = (animator.currentPlayTime / animator.duration * progress_bar.max).toInt()
                                animator.end()
                                if (progress == progress_bar.max) {
                                    if (currentUser != null) {
                                        startActivity(MainActivity.getIntent(this))
                                        finish()
                                    } else {
                                        startActivity(LoginActivity.getIntent(this))
                                        finish()
                                    }
                                } else {
                                    val delay = 1000L
                                    ObjectAnimator.ofInt(progress_bar, "progress", progress, progress_bar.max)
                                        .apply {
                                            duration = delay
                                            interpolator = DecelerateInterpolator()
                                        }
                                        .start()

                                    handler.postDelayed({
                                        if (currentUser != null) {
                                            startActivity(MainActivity.getIntent(this))
                                            finish()
                                        } else {
                                            startActivity(LoginActivity.getIntent(this))
                                            finish()
                                        }
                                    }, delay)
                                }
                            }
                        }
                        .connect()
                }
            }, {})
            .connect()
    }

    private fun getCurrentLocation(): Single<LatLng> =
        Single.create<LatLng> { emitter ->
            try {
                fusedLocationClientProvider.lastLocation
                    .addOnSuccessListener { location ->
                        if (emitter.isDisposed) {
                            return@addOnSuccessListener
                        }

                        if (location != null) {
                            emitter.onSuccess(LatLng(location.latitude, location.longitude))
                        } else {
                            emitter.onSuccess(App.currentLocation)
                        }
                    }
                    .addOnFailureListener {}
            } catch (exception: SecurityException) {
                emitter.onError(exception)
            }
        }

    private fun Disposable.connect() = disposable.add(this)

    override fun onDestroy() {
        handler.removeCallbacksAndMessages(null)
        disposable.dispose()
        super.onDestroy()
    }

    interface Splash {
        @GET("hockeyapp.json")
        fun init(): Single<JsonObject>
    }
}