package com.gohockey.ui.post

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.arellomobile.mvp.MvpAppCompatActivity
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.IceOffer
import com.gohockey.model.User
import com.gohockey.ui.global.BaseFragment
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.Command

class PostActivity : MvpAppCompatActivity() {
    companion object {
        fun getIntent(context: Context, iceOffer: IceOffer, user: User) = Intent(context, PostActivity::class.java).apply {
            putExtra("IceOffer", iceOffer)
            putExtra("User", user)
        }
    }

    private val currentFragment: BaseFragment?
        get() = supportFragmentManager.findFragmentById(R.id.content_layout) as? BaseFragment

    private val navigator: Navigator =
        object : SupportAppNavigator(this, supportFragmentManager, R.id.content_layout) {
            override fun setupFragmentTransaction(
                command: Command?,
                currentFragment: Fragment?,
                nextFragment: Fragment?,
                fragmentTransaction: FragmentTransaction
            ) {
                fragmentTransaction.setReorderingAllowed(true)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_container)

        if (savedInstanceState == null) {
            App.router.newRootScreen(Screens.Ice(
                intent.extras!!.getParcelable("IceOffer")!!,
                intent.extras!!.getParcelable("User")!!
            ))
        }
    }

    override fun onResume() {
        super.onResume()
        App.navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        App.navigatorHolder.removeNavigator()
    }

    override fun onBackPressed() {
        currentFragment?.onBackPressed() ?: super.onBackPressed()
    }
}

object Screens {
    data class Ice(
        private val iceOffer: IceOffer,
        private val user: User
    ) : SupportAppScreen() {
        override fun getFragment() = IceFragment.newInstance(iceOffer, user)
    }
}