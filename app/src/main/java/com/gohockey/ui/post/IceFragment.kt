package com.gohockey.ui.post

import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.gohockey.App
import com.gohockey.R
import com.gohockey.model.IceOffer
import com.gohockey.model.User
import com.gohockey.model.toIceOffer
import com.gohockey.ui.global.BaseFragment
import com.gohockey.ui.members.MembersActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import durdinapps.rxfirebase2.RxFirestore
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_ice.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.properties.Delegates

class IceFragment : BaseFragment() {
    companion object {
        private const val ARG_POST = "ice offer"
        private const val ARG_USER = "offer author"

        fun newInstance(iceOffer: IceOffer, user: User) = IceFragment().apply {
            arguments = Bundle().apply {
                putParcelable(ARG_POST, iceOffer)
                putParcelable(ARG_USER, user)
            }
        }
    }

    override val layoutRes = R.layout.fragment_ice

    private val currentUserId = FirebaseAuth.getInstance().currentUser?.uid
    private val state get() = arguments?.getParcelable(ARG_POST) as IceOffer
    private val author get() = arguments?.getParcelable(ARG_USER) as User

    private var membersSubscription: Disposable? = null
    private var addRemoveSubscription: Disposable? = null
    private var isUserInIce: Boolean by Delegates.observable(false) { property, oldValue, newValue ->
        updateButton()
    }
    private var members: List<String> by Delegates.observable(listOf()) { property, oldValue, newValue ->
        membersTextView?.text = "${newValue.size}"
    }

    private var membersWait: List<String> by Delegates.observable(listOf()) { property, oldValue, newValue ->
    }
    private var membersTextView: TextView? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Glide.with(this)
            .load(author.photo)
            .apply {
                placeholder(R.drawable.ic_user_placeholder_60)
                error(R.drawable.ic_user_placeholder_60)
                circleCrop()
            }
            .into(iceImage)

        iceToolbar.setNavigationOnClickListener {
            App.router.exit()
        }
        iceName.text = state.nameIce

        val dateFormatter = SimpleDateFormat("dd.MM.yyyy в HH:mm", Locale.getDefault())
        membersSubscription =
                RxFirestore.observeDocumentRef(FirebaseFirestore.getInstance()
                    .collection("IceOffers")
                    .document(state.id))
                        .map { it.data!!.toIceOffer() }
                        .map { it.members to it.membersWait }
                        .doOnSubscribe {
                            iceSignInInIce.isEnabled = false
                        }
                        .doOnNext {
                            iceSignInInIce.isEnabled = true
                        }
                        .distinctUntilChanged()
                        .subscribe { (iceMembers, iceMembersWait) ->
                            members = iceMembers
                            membersWait = iceMembersWait
                            isUserInIce = iceMembers.contains(currentUserId) || iceMembersWait.contains(currentUserId)
                        }
        iceSignInInIce.isVisible = state.userId != currentUserId
        iceSignInInIce.setOnRippleClickListener {
            addRemoveSubscription?.dispose()
            val resultMembers = when {
                membersWait.contains(currentUserId) -> membersWait.toMutableList().apply { remove(currentUserId) }
                members.contains(currentUserId) -> members.toMutableList().apply { remove(currentUserId) }
                else -> membersWait.toMutableList().apply { currentUserId?.let { add(currentUserId) } }
            }
            val updatedMembers = when {
                membersWait.contains(currentUserId) -> false
                members.contains(currentUserId) -> true
                else -> false
            }

            addRemoveSubscription = RxFirestore.updateDocument(
                    FirebaseFirestore.getInstance().collection("IceOffers").document(state.id),
                    if (updatedMembers) "members" else "membersWait",
                    resultMembers
            )
                    .doOnSubscribe {
                        iceSignInInIce.isEnabled = false
                    }
                    .doOnComplete {
                        iceSignInInIce.isEnabled = true
                    }.subscribe()
        }
        iceMembers.apply {
            setOnRippleClickListener {
                if (!members.isNullOrEmpty()) startActivity(MembersActivity.getIntent(context!!, state))
            }
            membersTextView = findViewById<TextView>(R.id.partItemDescription)
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemText).text = "Участники"
        }
        iceDateFrom.apply {
            findViewById<TextView>(R.id.partItemText).text = "Дата начала"
            findViewById<TextView>(R.id.partItemDescription).text = dateFormatter.format(state.startDate.toDate())
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }
        iceDateTo.apply {
            findViewById<TextView>(R.id.partItemText).text = "Дата окончания"
            findViewById<TextView>(R.id.partItemDescription).text = dateFormatter.format(state.endDate.toDate())
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }
        iceTheme.apply {
            findViewById<TextView>(R.id.partItemText).text = "Тематика"
            findViewById<TextView>(R.id.partItemDescription).text = state.theme
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }
        iceQualification.apply {
            findViewById<TextView>(R.id.partItemText).text = "Квалификация"
            findViewById<TextView>(R.id.partItemDescription).text = state.qualification
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }
        iceNeedGoalKeeper.apply {
            findViewById<TextView>(R.id.partItemText).text = "Есть ли вратарь"
            findViewById<SwitchCompat>(R.id.partItemSwitch).isChecked = state.goalKeeper
            findViewById<SwitchCompat>(R.id.partItemSwitch).isClickable = false
        }
        iceNeedCoach.apply {
            findViewById<TextView>(R.id.partItemText).text = "Есть ли тренер"
            findViewById<SwitchCompat>(R.id.partItemSwitch).isChecked = state.coach
            findViewById<SwitchCompat>(R.id.partItemSwitch).isClickable = false
        }
        iceMetro.apply {
            findViewById<TextView>(R.id.partItemText).text = "Где удобно играть"
            findViewById<TextView>(R.id.partItemDescription).text = App.subways.find { it.id == state.metroId }!!.name
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }
        icePrice.apply {
            findViewById<TextView>(R.id.partItemText).text = "Стоимость"
            findViewById<TextView>(R.id.partItemDescription).text = "${state.price} \u20BD"
            findViewById<TextView>(R.id.partItemDescription).isVisible = true
            findViewById<TextView>(R.id.partItemArrow).isVisible = false
        }
//        iceAddress.apply {
//            findViewById<TextView>(R.id.partItemText).text = "Адрес"
//            findViewById<TextView>(R.id.partItemDescription).text = state.metroId
//            findViewById<TextView>(R.id.partItemDescription).isVisible = true
//            findViewById<TextView>(R.id.partItemArrow).isVisible = false
//        }
    }

    private fun updateButton() {
        iceSignInInIce.text = if (isUserInIce) "Отказаться от участия" else "Принять участие"
    }

    override fun onBackPressed() {
        App.router.exit()
    }

    override fun onDestroyView() {
        addRemoveSubscription?.dispose()
        membersSubscription?.dispose()
        super.onDestroyView()
    }
}

private const val RIPPLE_EFFECT_DELAY = 150L

fun View.setOnRippleClickListener(listener: () -> Unit) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        setOnClickListener { postDelayed({ if (hasWindowFocus()) listener() }, RIPPLE_EFFECT_DELAY) }
    } else {
        setOnClickListener { listener() }
    }
}
