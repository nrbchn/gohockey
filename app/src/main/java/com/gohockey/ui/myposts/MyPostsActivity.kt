package com.gohockey.ui.myposts

import android.os.Bundle
import androidx.core.view.isVisible
import androidx.core.view.setPadding
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gohockey.R
import com.gohockey.model.Prefs
import com.gohockey.presentation.myposts.MyPostsPresenter
import com.gohockey.presentation.myposts.MyPostsView
import com.gohockey.ui.main.feed.IceOfferAdapterDelegate
import com.gohockey.ui.map.MapActivity
import com.gohockey.ui.post.PostActivity
import com.google.android.gms.location.LocationServices
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import kotlinx.android.synthetic.main.activity_my_posts.*
import kotlinx.android.synthetic.main.layout_base_list.*

class MyPostsActivity : MvpAppCompatActivity(), MyPostsView {

    private val prefs by lazy { Prefs(this) }
    private val fusedLocationClientProvider by lazy { LocationServices.getFusedLocationProviderClient(this) }
    private val adapter by lazy { FeedAdapter() }

    @InjectPresenter
    lateinit var presenter: MyPostsPresenter

    @ProvidePresenter
    fun providePresenter() = MyPostsPresenter(prefs, fusedLocationClientProvider)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_posts)

        toolbar.setNavigationOnClickListener { onBackPressed() }

        swipe_refresh.setOnRefreshListener { presenter.refresh() }

        with(recycler_view) {
            layoutManager = LinearLayoutManager(context)
            adapter = this@MyPostsActivity.adapter
            setPadding(resources.getDimensionPixelOffset(R.dimen.card_margin))
            clipToPadding = false
        }
    }

    override fun showData(data: List<Any>) {
        adapter.setData(data)
    }

    override fun showProgress(show: Boolean) {
        swipe_refresh.isRefreshing = show
    }

    override fun showError(message: String?) {
        error_text_view.isVisible = !message.isNullOrEmpty()
        error_text_view.text = message
    }

    private inner class FeedAdapter : ListDelegationAdapter<MutableList<Any>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(IceOfferAdapterDelegate(
                { startActivity(PostActivity.getIntent(this@MyPostsActivity, it.post, it.author)) },
                { startActivity(MapActivity.getIntent(this@MyPostsActivity, ArrayList(listOf(it)))) }
            ))
        }

        fun setData(data: List<Any>) {
            items.clear()
            items.addAll(data)
            notifyDataSetChanged()
        }
    }
}