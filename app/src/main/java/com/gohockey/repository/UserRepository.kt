package com.gohockey.repository

import com.gohockey.model.user.IdRole
import com.gohockey.model.user.User
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import durdinapps.rxfirebase2.RxFirestore
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.functions.Function3
import java.lang.RuntimeException

class UserRepository(
    private val firestore: FirebaseFirestore
) {

    fun getUser(id: String): Single<User> =
        RxFirestore.getDocument(firestore.collection("Users").document(id))
            .flatMap {
                val collection = firestore.collection(
                    when (it.getString("idRole")) {
                        IdRole.PLAYER.title -> "Player"
                        IdRole.GOALKEEPER.title -> "GoalKeeper"
                        IdRole.ADMIN_ICE.title -> "AdminIce"
                        else -> throw RuntimeException("No such field or idRole")
                    }
                )
                RxFirestore.getDocument(collection.document(id))
            }
            .map {
                when (it.getString("idRole")) {
                    IdRole.PLAYER.title -> User.Player.fromMap(it.data!!)
                    IdRole.GOALKEEPER.title -> User.Goalkeeper.fromMap(it.data!!)
                    IdRole.ADMIN_ICE.title -> User.AdminIce.fromMap(it.data!!)
                    else -> throw RuntimeException("No such field or idRole")
                }
            }
            .toSingle()

    fun getUsers(idRole: IdRole): Single<List<User>> =
        RxFirestore.getCollection(
            firestore.collection(
                when (idRole) {
                    IdRole.PLAYER -> "Player"
                    IdRole.GOALKEEPER -> "GoalKeeper"
                    IdRole.ADMIN_ICE -> "AdminIce"
                }
            )
        )
            .map {
                it.documents
                    .map {
                        when (idRole) {
                            IdRole.PLAYER -> User.Player.fromMap(it.data!!)
                            IdRole.GOALKEEPER -> User.Goalkeeper.fromMap(it.data!!)
                            IdRole.ADMIN_ICE -> User.AdminIce.fromMap(it.data!!)
                        }
                    }
                    .sortedBy { it.name + " " + it.surname }
            }
            .toSingle()

    fun getUsers(): Single<List<User>> = Maybe.zip(
        RxFirestore.getCollection(firestore.collection("Player")),
        RxFirestore.getCollection(firestore.collection("GoalKeeper")),
        RxFirestore.getCollection(firestore.collection("AdminIce")),
        Function3 { q1: QuerySnapshot, q2: QuerySnapshot, q3: QuerySnapshot ->
            mutableListOf<User>()
                .apply {
                    addAll(
                        q1.documents
                            .map { User.Player.fromMap(it.data!!) }
                    )
                    addAll(
                        q2.documents
                            .map { User.Goalkeeper.fromMap(it.data!!) }
                            .filter { it.isVisible }
                    )
                    addAll(
                        q3.documents
                            .map { User.AdminIce.fromMap(it.data!!) }
                    )
                }
                .sortedBy { it.name + " " + it.surname }
                .toList()
        }
    )
        .toSingle()
}