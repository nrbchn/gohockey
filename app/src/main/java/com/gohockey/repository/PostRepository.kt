package com.gohockey.repository

import com.gohockey.model.post.Post
import com.google.firebase.firestore.FirebaseFirestore
import durdinapps.rxfirebase2.RxFirestore
import io.reactivex.Maybe

class PostRepository(
    val firestore: FirebaseFirestore
) {

    fun getIceOffers(): Maybe<List<Post.IceOffer>> =
        RxFirestore.getCollection(firestore.collection("IceOffers"))
            .map {
                it.documents.map {
                    Post.IceOffer.fromMap(it.data ?: throw RuntimeException("No such collection or document"))
                }
            }

    fun getIceOffer(id: String): Maybe<Post.IceOffer> =
        RxFirestore.getDocument(firestore.collection("IceOffers").document(id))
            .map {
                Post.IceOffer.fromMap(it.data ?: throw RuntimeException("No such collection or document"))
            }
}